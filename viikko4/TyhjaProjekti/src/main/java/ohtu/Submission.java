package ohtu;

import static java.lang.Boolean.TRUE;
import java.util.ArrayList;
import java.util.List;

public class Submission {
    private String student_number;
    private String week;
    private String hours;
    
    private Boolean a1;
    private Boolean a2;
    private Boolean a3;
    private Boolean a4;
    private Boolean a5;
    private Boolean a6;
    private Boolean a7;
    private Boolean a8;
    private Boolean a9;
    private Boolean a10;
    private Boolean a11;
    private Boolean a12;
    private Boolean a13;
    private Boolean a14;
    private Boolean a15;
    
    public List<Integer> exercise_list = new ArrayList<Integer>();
    
    public String getStudent_number() {
        return student_number;
    }
    
    public List<Integer> exercise_lister_int(){
        
        if (a1 == TRUE) {
            exercise_list.add(1);
        }
        if (a2 == TRUE) {
            exercise_list.add(2);
        }
        if (a3 == TRUE) {
            exercise_list.add(3);
        }
        if (a4 == TRUE) {
            exercise_list.add(4);
        }
        if (a5 == TRUE) {
            exercise_list.add(5);
        }
        if (a6 == TRUE) {
            exercise_list.add(6);
        }
        if (a7 == TRUE) {
            exercise_list.add(7);
        }
        if (a8 == TRUE) {
            exercise_list.add(8);
        }
        if (a9 == TRUE) {
            exercise_list.add(9);
        }
        if (a10 == TRUE) {
            exercise_list.add(10);
        }
        if (a11 == TRUE) {
            exercise_list.add(11);
        }
        if (a12 == TRUE) {
            exercise_list.add(12);
        }
        if (a13 == TRUE) {
            exercise_list.add(13);
        }
        if (a14 == TRUE) {
            exercise_list.add(14);
        }
        if (a15 == TRUE) {
            exercise_list.add(15);
        }
        return  exercise_list;
    }   
    
    public int listLength () {
        return exercise_list.size();
    }
    
    public void setStudent_number(String student_number) {
        this.student_number = student_number;
    }
    
    public int getHours (){
        return Integer.parseInt(hours);
    }
       
    @Override
    public String toString() {
        
        String stringi = exercise_lister_int().toString();
        stringi = stringi.substring(1, stringi.length()-1);
        
        return " viikko " + week + ": tehtyjä tehtäviä yhteensä: " + listLength() + ", aikaa kului " + hours + " tuntia, " + stringi;
    } 
    
}