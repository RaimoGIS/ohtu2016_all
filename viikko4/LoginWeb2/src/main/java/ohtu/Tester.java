package ohtu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Tester {
    public static void main(String[] args) {
        WebDriver driver = new HtmlUnitDriver();

        driver.get("http://localhost:8090");
        System.out.println( driver.getPageSource() );
        WebElement element = driver.findElement(By.linkText("login"));       
        element.click(); 
        //
        System.out.println("==");
        
        System.out.println( driver.getPageSource() );
        element = driver.findElement(By.name("username"));
        element.sendKeys("pekka");
        element = driver.findElement(By.name("password"));
        element.sendKeys("akkep");
        element = driver.findElement(By.name("login"));
        element.submit();
        
        System.out.println("==");
        System.out.println( driver.getPageSource() );
        
        
        System.out.println("=back to start page=");
        driver.get("http://localhost:8090");
        //System.out.println( driver.getPageSource() );    
        element = driver.findElement(By.linkText("login"));
        element.click();         
        
        System.out.println("=Try wrong password=");
        
        element = driver.findElement(By.name("username"));
        element.sendKeys("pekka");
        element = driver.findElement(By.name("password"));
        element.sendKeys("akkepekka");
        element = driver.findElement(By.name("login"));
        element.submit();
        System.out.println( driver.getPageSource() );
        
        
        System.out.println("=back to start page=");
        driver.get("http://localhost:8090");
        //System.out.println( driver.getPageSource() );    
        element = driver.findElement(By.linkText("login"));
        element.click();         
        
        System.out.println("=Try wrong username=");
        
        element = driver.findElement(By.name("username"));
        element.sendKeys("raimo");
        element = driver.findElement(By.name("password"));
        element.sendKeys("akkep");
        element = driver.findElement(By.name("login"));
        element.submit();
        System.out.println( driver.getPageSource() );

        
        
        System.out.println("=back to start page=");
        driver.get("http://localhost:8090");    
        element = driver.findElement(By.linkText("register new user"));
        element.click(); 
        
        
        System.out.println("=Create new user=");
        
        element = driver.findElement(By.name("username"));
        element.sendKeys("raimo24");
        element = driver.findElement(By.name("password"));
        element.sendKeys("testtest1");
        element = driver.findElement(By.name("passwordConfirmation"));
        element.sendKeys("testtest1");
        element = driver.findElement(By.name("add"));
        element.submit();
        System.out.println( driver.getPageSource() );
        System.out.println("=New user created?=");
        
        
        System.out.println("=Continue to application=");
        element = driver.findElement(By.linkText("continue to application mainpage"));
        element.click();         
        System.out.println( driver.getPageSource() );
        
        System.out.println("=logout=");
        element = driver.findElement(By.linkText("logout"));
        element.click();         
        System.out.println( driver.getPageSource() );
    }
}
