## Ensimmäisen sprintin arvosteluperusteet:

Projekti tulee olla rekisteröity osoitteeseen [http://ohtustats2016.herokuapp.com/miniprojects](http://ohtustats2016.herokuapp.com/miniprojects)

Linkit projektin backlogeihin ja muihin dokumentteihin, ja travisiin tulee laittaa projektin githubin README:hen!

### Täysiin pisteisiin (2p) vaaditaan:

* product backlog 
  * backlog on DEEP (storyjä ei tarvitse estimoida)
* sprintin 1 backlog
  * sprintiin valitut user storyt jaettu teknisen tason taskeiksi
  * päivittäinen jäjellä oleva työmäärä arvioitu taskeittain
  * burndown-käyrä olemassa
* sprintiin 1 valittujen storyjen hyväksymisehdot kirjattu
* testaus
  * kaikki toteutettu koodi melko kattavasti testattua
  * yksikkötestit
  * ainakin jossain storyssä hyväksymäehtojen testausta (easyB)	
* jatkuva integraatio
  * koodi githubissa
* definition of done kirjattu eksplisiittisesti
* toteutus
  * suurin osa sprintin tavoitteeseen sovituista storyistä toteutettu Definition of donen mukaisella tasolla
* työtä tehty tasaisesti
  * kaikki ei saa olla yhtenä päivänä tehty
* toimiva, demossa näytettävä versio on _tagatty_ (tagilla sprintti1) ja siitä on luotu GitHubiin [release](https://help.github.com/articles/creating-releases/). Jos kyseessä on konsolisovellus, releaseen liitetään projektin ajettava jar-tiedosto

## Sprintin 2 arvosteluperusteet:

### Täysiin pisteisiin (2p) vaaditaan:

* kattava testaus yksikkö- ja storytasolla
* product backlog asiallisessa kunnossa
  * sisältää user storyt priorisoituna
  * estimointia ei vaadita
* sprint backlog asiallisessa kunnossa
  * sisältää valittujen storyen toteuttamiseen edellytettävät taskit
  * taskien jäljelläolevaa työtä on arvioitu lähes päivittäin
* sprintin burndown olemassa 
* jatkuva integraatio
* työtä on tehty tasaisesti
* saadaan tehtyä bibtex, joka toimii latexin kanssa yhteen projektisivun esimerkillä,
formaatit article, book, inproceedings tuettuina
* toimiva, demossa näytettävä versio on _tagatty_ (tagilla sprintti1) ja siitä on luotu GitHubiin [release](https://help.github.com/articles/creating-releases/). Jos kyseessä on konsolisovellus, releaseen liitetään projektin ajettava jar-tiedosto

