Kurssilla käsitellään ohjelmistotuotantoprojektien hallinnan, työvaiheiden ja työvälineiden perusteita. Erityinen painotus ketterissä ohjelmistotutantomenetelmissä.

Kurssilla sisältää luentoja 4 tuntia viikossa, viikottaiset harjoitustehtävät ja ns. miniprojektin

**kurssin opetusjärjestelyt ja arvosteluperusteet** on kuvattu tarkemmin [luennon 1 kalvoilta](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento1.pdf?raw=true)

## Ajankohtaista

* [Miniprojekti](https://github.com/mluukkai/ohtu2016/wiki/miniprojekti) käynnissä
* ohjausta tehtävien tekoon torstaisin klo 14-17 B221
* kurssin irc-kanava [#ohtu2016](http://chat.mibbit.com/?server=ircnet.eversible.com&channel=%23ohtu2016)
  *  **Huom:** kaikki epäasialliset, halventavat ja jotain ihmisryhmää syrjivät kommentit kanavalla ovat kiellettyjä ja tälläisten kommenttien esittäjät poistetaan kanavalta

## Linkkejä
* luentokalvot [1](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento1.pdf?raw=true) [2](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento2.pdf?raw=true) [3](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento3.pdf?raw=true) [4](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento4.pdf?raw=true) [5](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento5.pdf?raw=true) [6](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento6.pdf?raw=true) [7](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento7.pdf?raw=true) [8](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento8.pdf?raw=true) [9](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento9.pdf?raw=true) [10](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento10.pdf?raw=true)
* laskarit: [1](https://github.com/mluukkai/ohtu2016/wiki/laskari-1) [2](https://github.com/mluukkai/ohtu2016/wiki/laskari-2) [3](https://github.com/mluukkai/ohtu2016/wiki/laskari-3) [4](https://github.com/mluukkai/ohtu2016/wiki/laskari-4)
* luentoihin liittyvä koodi [8](https://github.com/mluukkai/ohtu2016/blob/master/web/luento8.md) [9](https://github.com/mluukkai/ohtu2016/blob/master/web/luento9.md)
* tehtävien palautus [http://ohtustats2016.herokuapp.com/](http://ohtustats2016.herokuapp.com/)


## Viikko 1

### luento 1: ma 14.3. B123 klo 14-16

* aiheina
  * johdanto, ohjelmistoprosessin vaiheet, prosessimallit
  * tekniset asiat: versionhallinnan ja buildhallinnan alkeet
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento1.pdf?raw=true)
* taustamateriaalia:
  * Martin Fowler The New Methodology [http://martinfowler.com/articles/newMethodology.html](http://martinfowler.com/articles/newMethodology.html)
  * Manifesto for Agile Software Development [http://agilemanifesto.org/](http://agilemanifesto.org/)
  * Version control [http://jamesshore.com/Agile-Book/version_control.html](http://jamesshore.com/Agile-Book/version_control.html)
  * Ten minute build [http://jamesshore.com/Agile-Book/ten_minute_build.html](http://jamesshore.com/Agile-Book/ten_minute_build.html)

### luento 2: ti 15.3. CK112 klo 12-14

* aiheina
  * ketterä/iteratiivinen ohjelmistoprosessi: scrum
  * design pattern of the day: dependency injection
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento2.pdf?raw=true)
* taustamaterialia:
  * <http://www.scrumprimer.org/scrumprimer20.pdf>
  * [http://www.scrumguides.org/](http://www.scrumguides.org/)
  * [http://www.martinfowler.com/bliki/FlaccidScrum.html](http://www.martinfowler.com/bliki/FlaccidScrum.html)
  * [http://jamesshore.com/Blog/Dependency-Injection-Demystified.html](http://jamesshore.com/Blog/Dependency-Injection-Demystified.html)

### Laskarit 1

* [tehtävät](Laskari-1) deadline su 20.3. klo 23.59

## Viikko 2

*HUOM: pääsisiloma 24.-30.3.*

### luento 3: ma 21.3. B123 klo 14-16

* aiheina
  * vaatimusmäärittely
  * ketterä vaatimustenhallinta ja projektisuunnittelu, osa 1:
    * vaatimusten kerääminen
    * vaatimusten dokumentointi: user storyt
    * estimointi
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento3.pdf?raw=true)
* taustamateriaalia
  * Sommerville: [Integrated requirements engineering: a tutorial](http://ieeexplore.ieee.org/search/srchabstract.jsp?tp=&arnumber=1377118&openedRefinements%3D*%26filter%3DAND%28NOT%284283010803%29%29%26searchField%3DSearch+All%26queryText%3DIntegrated+requirements+engineering%3A+a+tutorial)
  * **huom:** artikkeli downloadattavissa vain laitoksen verkosta. paikallinen kopio [täällä](https://www.cs.helsinki.fi/i/mluukkai/sommerville.pdf)
  * [Lean startup](http://theleanstartup.com/principles)
  * Kniberg: [Scrum and XP form the trenches](http://www.infoq.com/minibooks/scrum-xp-from-the-trenches) sivut 9-55
  * Shore: Art of agile development, luvut [release planning](http://www.jamesshore.com/Agile-Book/release_planning.html) ja [stories](http://www.jamesshore.com/Agile-Book/stories.html)
  * Rasmussen: The Agile Samurai, luvut 6-8
  * [http://xp123.com/articles/invest-in-good-stories-and-smart-tasks/](http://xp123.com/articles/invest-in-good-stories-and-smart-tasks/)

### luento 4: ti 22.3. CK112 klo 12-14

* aiheina
  * vaatimusmäärittely
    * hyvän backlogin tunnusmerkit
    * release planning
  * ketterä vaatimustenhallinta ja projektisuunnittelu, osa 2:
    * iteration suunnittelu
    * edistymisen raportointi
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento4.pdf?raw=true)
* taustamateriaalia
  * [http://www.romanpichler.com/blog/product-backlog/making-the-product-backlog-deep/](http://www.romanpichler.com/blog/product-backlog/making-the-product-backlog-deep/)
  * [http://www.romanpichler.com/blog/product-backlog/grooming-the-product-backlog/](http://www.romanpichler.com/blog/product-backlog/grooming-the-product-backlog/)
  * Kniberg: [Scrum and XP form the trenches](http://www.infoq.com/minibooks/scrum-xp-from-the-trenches) sivut 9-55
  * [http://www.richardlawrence.info/2009/10/28/patterns-for-splitting-user-stories/](http://www.richardlawrence.info/2009/10/28/patterns-for-splitting-user-stories/)
  * [http://www.mountaingoatsoftware.com/scrum/sprint-planning-meeting](http://www.mountaingoatsoftware.com/scrum/sprint-planning-meeting)
  * [http://jamesshore.com/Agile-Book/the_planning_game.html](http://jamesshore.com/Agile-Book/the_planning_game.html)
  * [http://www.mountaingoatsoftware.com/scrum/sprint-backlog](http://www.mountaingoatsoftware.com/scrum/sprint-backlog)

### Laskarit 2

* [tehtävät](Laskari-2) deadline su 3.4. klo 23.59

## Viikko 3

*HUOM: pääsisiloma 24.-30.3.*

### luento 5: ma 4.4. B123 klo 14-16

* aiheet
  * testaus, osa 1
    * verifiointi ja validointi
    * katselmoinnit ja tarkastukset
    * testauksen tasot (yksikkö-, integraatio- ja järjestelmätestaus)
    * testitapausten valinta
    * testauksen kattavuuden mittaus
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento5.pdf?raw=true)
* taustamateriaalia
  * [http://jamesshore.com/Agile-Book/pair_programming.html](http://jamesshore.com/Agile-Book/pair_programming.html)
  * [http://jamesshore.com/Agile-Book/collective_code_ownership.html](http://jamesshore.com/Agile-Book/collective_code_ownership.html)
  * [http://jamesshore.com/Agile-Book/coding_standards.html](http://jamesshore.com/Agile-Book/coding_standards.html)
  * [http://c2.com/cgi/wiki?InternalAndExternalQuality](http://c2.com/cgi/wiki?InternalAndExternalQuality)
  * [http://en.wikipedia.org/wiki/Mutation_testing](http://en.wikipedia.org/wiki/Mutation_testing)

### luento 6: ti 5.4. CK112 klo 12-14

* aiheet
  * testaus, osa 2
    * TDD
    * User Storyjen automatisoitu hyväksymätestaus
    * Continuous integration, continuous delivery, continuous deployment
    * tutkiva testaaminen
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento6.pdf?raw=true)
* taustamateriaalia
  * [http://jamesshore.com/Agile-Book/test_driven_development.html](http://jamesshore.com/Agile-Book/test_driven_development.html)
  * [http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd)
  * [http://martinfowler.com/articles/mocksArentStubs.html](http://martinfowler.com/articles/mocksArentStubs.html)
  * [http://testobsessed.com/wp-content/uploads/2011/04/atddexample.pdf](http://testobsessed.com/wp-content/uploads/2011/04/atddexample.pdf)
  * [http://www.methodsandtools.com/archive/archive.php?id=23](http://www.methodsandtools.com/archive/archive.php?id=23)
  * [http://www.methodsandtools.com/archive/archive.php?id=72](http://www.methodsandtools.com/archive/archive.php?id=72)
  * [http://www.industriallogic.com/papers/storytest.pdf](http://www.industriallogic.com/papers/storytest.pdf)
  * [http://dannorth.net/introducing-bdd/](http://dannorth.net/introducing-bdd/)
  * [http://seleniumhq.org/docs/03_webdriver.html](http://seleniumhq.org/docs/03_webdriver.html)
  * [http://www.stevemcconnell.com/ieeesoftware/bp04.htm](http://www.stevemcconnell.com/ieeesoftware/bp04.htm)
  * [http://martinfowler.com/articles/continuousIntegration.html](http://martinfowler.com/articles/continuousIntegration.html)
  * [http://jamesshore.com/Agile-Book/continuous_integration.html](http://jamesshore.com/Agile-Book/continuous_integration.html)
  * [http://www.thoughtworks.com/insights/blog/case-continuous-delivery](http://www.thoughtworks.com/insights/blog/case-continuous-delivery)
  * [http://www.satisfice.com/articles/et-article.pdf](http://www.satisfice.com/articles/et-article.pdf)
  * [http://www.satisfice.com/articles/what_is_et.shtml](http://www.satisfice.com/articles/what_is_et.shtml)

### laskarit

* [tehtävät](Laskari-3) deadline su 10.4. klo 23.59

### miniprojektin aloitus

* [https://github.com/mluukkai/ohtu2016/wiki/miniprojekti](https://github.com/mluukkai/ohtu2016/wiki/miniprojekti)

## Viikko 4

### luento 7: ma 11.4. B123 klo 14-16

* aiheet
  * ohjelmiston arkkitehtuuri
  * arkkitehtuuri ketterissä menetelmissä
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento7.pdf?raw=true)
* taustamateriaalia
  * [http://msdn.microsoft.com/en-us/architecture/ff476940](http://msdn.microsoft.com/en-us/architecture/ff476940)
  * [http://jamesshore.com/Agile-Book/incremental_design.html](http://jamesshore.com/Agile-Book/incremental_design.html)
  * [http://butunclebob.com/ArticleS.UncleBob.IncrementalArchitecture](http://butunclebob.com/ArticleS.UncleBob.IncrementalArchitecture)
  * [http://martinfowler.com/articles/designDead.html](http://martinfowler.com/articles/designDead.html)

### luento 8: ti 12.4. CK112 klo 12-14

* aiheet
  * ohjelmiston suunnittelu osa 1
    * oliosuunnittelun periaatteita
    * suunnittelumalleja: composed method, static factory, strategy, command, template method
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento8.pdf?raw=true)
* [kalvoihin liittyvä koodi](https://github.com/mluukkai/ohtu2016/blob/master/web/luento8.md)
* taustamateriaalia
  * [http://www.ibm.com/developerworks/java/library/j-eaed4/index.html](http://www.ibm.com/developerworks/java/library/j-eaed4/index.html)
  * [http://www.objectmentor.com/resources/articles/srp.pdf](http://www.objectmentor.com/resources/articles/srp.pdf)
  * [http://www.artima.com/lejava/articles/designprinciples.html](http://www.artima.com/lejava/articles/designprinciples.html)
  * [http://www.oodesign.com/strategy-pattern.html](http://www.oodesign.com/strategy-pattern.html)
  * [http://sourcemaking.com/design_patterns/strategy](http://sourcemaking.com/design_patterns/strategy)
  * [http://www.oodesign.com/command-pattern.html](http://www.oodesign.com/command-pattern.html)
  * [http://sourcemaking.com/design_patterns/command](http://sourcemaking.com/design_patterns/command)
  * [http://www.oodesign.com/template-method-pattern.html](http://www.oodesign.com/template-method-pattern.html)
  * [http://www.netobjectives.com/PatternRepository/index.php?title=TheTemplateMethodPattern](http://www.netobjectives.com/PatternRepository/index.php?title=TheTemplateMethodPattern)

### Laskarit 4

* [tehtävät](Laskari-4) deadline su 17.4. klo 23.59

### miniprojekti: sprintin 1 demo ja sprintin 2 suunnittelu

## Viikko 5

### luento 9: ma 18.4. B123 klo 14-17

* aiheet
  * ohjelmiston suunnittelu osa 2
    * suunnittelumalleja: dekoraattori, komposiitti, proxy, adapteri, fasaadi, MVC, observer
    * domain driven design
    * DSL
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento9.pdf?raw=true)
* [kalvoihin liittyvä koodi](https://github.com/mluukkai/ohtu2016/blob/master/web/luento9.md)
* taustamateriaalia
  * [http://sourcemaking.com/design_patterns/decorator](http://sourcemaking.com/design_patterns/decorator)
  * [http://sourcemaking.com/design_patterns/builder](http://sourcemaking.com/design_patterns/builder)
  * [http://martinfowler.com/bliki/FluentInterface.html](http://martinfowler.com/bliki/FluentInterface.html)
  * [http://www.infoq.com/articles/internal-dsls-java](http://www.infoq.com/articles/internal-dsls-java)
  * [http://sourcemaking.com/design_patterns/composite](http://sourcemaking.com/design_patterns/composite)
  * [http://sourcemaking.com/design_patterns/proxy](http://sourcemaking.com/design_patterns/proxy)
  * [http://sourcemaking.com/design_patterns/adapter](http://sourcemaking.com/design_patterns/adapter)
  * [http://www.infoq.com/articles/ddd-evolving-architecture](http://www.infoq.com/articles/ddd-evolving-architecture)
  * <http://www.infoq.com/articles/ddd-evolving-architecture>
  * <http://martinfowler.com/eaaCatalog/serviceLayer.html>
  * <http://sourcemaking.com/design_patterns/facade>
  * <http://sourcemaking.com/design_patterns/observer>

### luento 10: ti 19.4. CK112 klo 12-14

* aiheet
  * ohjelmiston suunnittelu osa 3
    * koodinhajut
    * refaktorointi
  * käytetäänkö ketteriä menetelmiä ja toimivatko ne?
  * katsaus kokeen kannalta tärkeimpiin asioihin
* [kalvot](https://github.com/mluukkai/ohtu2016/blob/master/kalvot/luento10.pdf?raw=true)
* taustamateriaalia
  * [http://www.infoq.com/articles/technical-debt-levison](http://www.infoq.com/articles/technical-debt-levison)
  * [http://blogs.construx.com/blogs/stevemcc/archive/2007/11/01/technical-debt-2.aspx](http://blogs.construx.com/blogs/stevemcc/archive/2007/11/01/technical-debt-2.aspx)
  * <http://martinfowler.com/bliki/TechnicalDebtQuadrant.html>
  * [http://sourcemaking.com/refactoring/bad-smells-in-code](http://sourcemaking.com/refactoring/bad-smells-in-code)
  * [http://c2.com/xp/CodeSmell.html](http://c2.com/xp/CodeSmell.html)
  * [http://wiki.java.net/bin/view/People/SmellsToRefactorings](http://wiki.java.net/bin/view/People/SmellsToRefactorings)
  * [http://www.codinghorror.com/blog/2006/05/code-smells.html](http://www.codinghorror.com/blog/2006/05/code-smells.html)
  * [http://sourcemaking.com/refactoring](http://sourcemaking.com/refactoring)
  * [Survey on agile and lean usage in Finnish software industry](http://dl.acm.org/citation.cfm?id=2372251.2372275&coll=DL&dl=GUIDE)

### Laskarit 5

* [tehtävät](Laskari-5) deadline su 24.4. klo 23.59

### miniprojekti: sprintin 2 demo ja sprintin 3 suunnittelu

## Viikko 6

*viikolla ei luentoja*

### Laskarit 6

* [tehtävät](Laskari-6) deadline su 1.5. klo 23.59

### miniprojekti: sprintin 3 demo ja sprintin 4 suunnittelu

## Viikko 7

*viikolla ei luentoja*

### Laskarit 7

* [tehtävät](Laskari-7) deadline su 8.5. klo 23.59

### miniprojekti, loppudemot

* miniprojektin kolmannen sprintin demot myöhemmin ilmoitettavana ajankohtana

## kurssikoe

* Tiistaina 10.5. salissa A111 klo 16.00