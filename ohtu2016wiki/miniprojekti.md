## Ajankohtaista

* [ensimmäisen sprintin arvosteluperusteet](https://github.com/mluukkai/ohtu2016/wiki/Miniprojektin-arvosteluperusteita#sprintin-1-arvosteluperusteet)
* [toisen sprintin arvosteluperusteet](https://github.com/mluukkai/ohtu2016/wiki/Miniprojektin-arvosteluperusteita#sprintin-2-arvosteluperusteet)
* [ilmoittautuminen sprinttien 2-4 asiakastapaamiseen](https://docs.google.com/document/d/1YfrhtcZt9n19NWn44HIjgnbkudxjDk_jeq9JR2YxshQ/edit)
* [projektien rekisteröinti](http://ohtustats2016.herokuapp.com/miniprojects)

## Johdanto

* Kurssin viikoilla 3-6 tehdään miniprojekti
* **kurssin läpipääsy edellyttää hyväksyttyä osallistumista miniprojektiin**
  * miniprojektiin osallistuminen ei ole välttämätöntä jos täytät työkokemuksen perusteella tapahtuvan Ohjelmistotuotantoprojektin hyväksiluvun edellyttävät kriteerit ks. kohta "Laaja suoritus" [sivulta](http://www.cs.helsinki.fi/opiskelu/tietotekniikka-alan-ty-kokemus-opintosuorituksena) 
  * jos "hyväksiluet" miniprojektin työkokemuksella, kerro asiasta välittömästi emailitse
* Projekti tehdään noin 4-5 hengen ryhmissä
* Projektissa ohjelmoidaan jonkin verran, pääpaino ei kuitenkaan ole ohjelmoinnissa vaan "prosessin" (tästä lisää myöhemmin) noudattamisessa
* **jokaisen ryhmän jäsenen on tarkoitus tehdä kunkin sprintin aikana töitä noin 4 tuntia projektin eteen**
* ryhmä tekee kussakin sprintissä sen minkä se sprinttiin varatussa ajassa pystyy tekemään, ei enempää eikä vähempää

## Ryhmän muodostaminen

* Ryhmä muodostetaan "spontaanisti", ma 4.4. ja ti 5.4. luennolla tai tulemalla paikalle ti 5.4. klo 15 C221, ke 6.4. klo 17 A218 tai pe 8.4. klo 14 C221
* Ryhmä keksii itselleen nimen, luo Github-repositorion ja rekisteröi itsensä [tänne](http://ohtustats2016.herokuapp.com/miniprojects)
* ryhmä varaa ajan asiakastapaamiselle [täältä](https://docs.google.com/document/d/1iF7-T3dVkNs-q8H8IwtwIJlvn0E-IcDPLLwh8e8Mdno/edit?usp=sharing)
* asiakastapaamisia pidetään tiistaina, keskiviikkona, torstaina ja perjantaina
* kun tulet asiakastapaamiseen, sinun tulee tuntea viikolla 2 käsitellyt termit **User story, Product backlog ja Sprint backlog** 

## Työn eteneminen

### viikko 3 (4-8.4)

* ryhmä muodostuu/muodostetaan
* ryhmät tapaavat asiakkaan (ti-pe)
  * tapaamisaikojen varaaminen [täällä](https://docs.google.com/document/d/1iF7-T3dVkNs-q8H8IwtwIJlvn0E-IcDPLLwh8e8Mdno/edit?usp=sharing)
* asiakaspalaverin pohjalta ryhmä tekee alustavan product backlogin ja sopii asiakkaan kanssa ensimmäisen sprintin tavoitteesta
* ryhmä suunnittelee ensimmäisen sprintin ja aloittaa työskentelyn
  * sprintin suunnittelun tuloksena ryhmä tekee sprint backlogin
* sprintin 1 [arvosteluperusteet](https://github.com/mluukkai/ohtu2016/wiki/Miniprojektin-arvosteluperusteita#sprintin-1-arvosteluperusteet)
* ryhmät varaavat tapaamisen seuraavien viikkojen asiakastapaamisille
  * tapaamisaikojen varaaminen [täältä](https://docs.google.com/document/d/1YfrhtcZt9n19NWn44HIjgnbkudxjDk_jeq9JR2YxshQ/edit)

### viikko 4 (11-15.4)

* ryhmät tapaavat asiakkaan (ti-pe)
  * tapaamisaikojen varaaminen [täältä](https://docs.google.com/document/d/1YfrhtcZt9n19NWn44HIjgnbkudxjDk_jeq9JR2YxshQ/edit)
* asiakaspalaverin pohjalta ryhmä ajantasaistaa product backlogin ja sopii asiakkaan kanssa toisen sprintin tavoitteesta
* ryhmä suunnittelee toisen sprintin ja työskentelee sen tavoitteiden saavuttamiseksi


#### asiakkaan toive sprinttiin 2

* toisen sprintin jälkeen tulee ohjelman kyetä tuottamaan bibtex-tiedosto, joka toimii yhteen osoitteessa
       [https://github.com/mluukkai/ohtu2013/blob/master/ohtu-bibtex.zip?raw=true](https://github.com/mluukkai/ohtu2013/blob/master/ohtu-bibtex.zip?raw=true) olevan latex-dokumentin kanssa
* dokumentti "käännetään" suorittamalla komennot <code>pdflatex paper; bibtex paper; pdflatex paper; pdflatex paper</code>
* tämän jälkeen tiedostossa __paper.pdf__ tulisi kaikkien lähdeviitteiden toimia, eli tiedostosta ei saa löytyä yhtään [?]:tä
* viitteiden merkkijonotunnisteiden (eli bibtex-itemin ylimmän rivin merkkijonon, mihin artikkelin tekstissä viitataan \cite:llä) ei tarvitse olla samat kuin artikkelissa käyteytyt, niiden tulee mielellään olla käyttelijän vapaasti määrittelemät tai oletusarvoisesti muodostetut
* luodun bibitex-tiedoston nimen tulee olla valittavissa (esimerkkiprojektissa nimen tulee olla __sigproc.bib__, aina ei kuitenkaan ole näin)
* huom: saatat joutua asentamaan fonttipaketin texlive-fonts-recommended, debianpohjaisissa linuxeissa (mm ubuntu) komennolla <code>sudo apt-get install texlive-fonts-recommended</code>


### viikko 5 (18-22.4)

* ryhmät tapaavat asiakkaan (ti-pe)
* asiakaspalaverin pohjalta ryhmä ajantasaistaa product backlogin ja sopii asiakkaan kanssa kolmanne sprintin tavoitteesta
* ryhmä suunnittelee kolmannen sprintin ja työskentelee sen tavoitteiden saavuttamiseksi


### viikko 6 (25-29.4)

* ryhmät tapaavat asiakkaan (ti-pe)
* asiakaspalaverin pohjalta ryhmä ajantasaistaa product backlogin ja sopii asiakkaan kanssa kolmanne sprintin tavoitteesta
* ryhmä suunnittelee neljännen sprintin ja työskentelee sen tavoitteiden saavuttamiseksi

### viikko 7 (2-6.5)

* ryhmä esittelee projektinsa lopputuloksen
* loppudemot tulevat olemaan xxx

## Toteutettava ohjelmisto

* asiakkaan visio tuotteesta [täällä](https://github.com/mluukkai/ohtu2016/wiki/miniprojekti-speksi)
* vaatimukset sovitaan ja niitä tarkennetaan viikoittaisissa palavereissa

## Tekniset ja prosessiin liittyvät vaatimukset

* Ryhmä laatii yhdessä asiakkaan kanssa Product backlogin
  * Vaatimukset kirjataan backlogiin User story:inä
* Sprintin suunnittelussa ryhmä sitoutuu toteuttamaan Backlogin kärjessä olevat User storyt
  * jokaisen ryhmäläisen "työaika" on 4 tuntia viikossa
  * ryhmä sitoutuu ainoastaan niihin User storyihin, jotka se kuvittelee kykenevänsä toteuttamaan sprintissä alla olevan   **Definition of Donen** mukaan
* ryhmä ylläpitää Sprint Backlogia
  * User storyt jaetaan sprintin suunnittelussa teknisen tason tehtäviksi
  * ryhmä tekee päivittäin jäljellä olevan työajan arviointia ja dokumentoi tämän Sprintin Burndown-käyränä
  * Koodi on talletettu GitHub:iin 
* Ryhmä toteuttaa jatkuvaa integraatiota (Continuous Integration) <https://travis-ci.org/> 

missä formaatissa product backlog ja sprint backlog pidetään?

* esim. Google Docs -spreadsheetinä, mallia voi ottaa seuraavista:
 * <http://www.mountaingoatsoftware.com/scrum/sprint-backlog>
 * erään ohtuprojektin [backlogit](https://docs.google.com/spreadsheet/ccc?key=0Aul6gm-diqbXdExfcHh0R1RGb18zbUMyblZvR09TUXc&usp=drive_web#gid=1)
* **Ryhmä lisää linkit backlogeihin, travisiin ja sovelluksen toimivaan versioon (jos sovellus on verkossa) miniprojektin Github-repositorion readmeen**

### definition of done

* User storyille on määriteltävä hyväksymäkriteerit, jotka dokumentoidaan easyB:n Storyjen skenaarioiksi
 * jokaista User storyä kohti siis pitää pääsääntöiseti olla oma easyB-story (esitellään viikolla 3)
* Toteututun koodin testikattavuus tulee olla mielellään 100% (rivikattavuus)
* Asiakas pääsee näkemään koko ajan koodin ja testien tilanteen CI-palvelimelta
* koodin ylläpidettävyyden tulee olla mahdollisimman hyvä
  * järkevä nimeäminen
  * järkevä/selkeä ja perusteltu arkkitehtuuri
  * hyvän oliosuunnittelun periaatteita (luennot 7 ja 8) tulee noudattaa

## Työn arvostelu

* miniprojektista saa maksimissaan 10 kurssipistettä (muut laskarit 10 pistettä ja koe 20 pistettä)
 * sprinteistä 1 ja 2 on jaossa 2 kurssipistettä, sprintistä 3 ja 4 jaossa 3 kurssipistettä
 * ensisijainen arvostelukriteeti on ohjelmaan toteutettujen featureiden laatu, tasainen eteneminen ja prosessin seuraaminen (ks. edellinen luku)
* osa jaossa olevista pisteistä perustuu henkilökohtaiseen suoritukseen, eli passiivinen osallistuminen ryhmään ei tuo automaattisesti samoja pisteitä kuin aktiivisille ryhmäläisille 

Täysiin pisteisiin kunkin viikon sprintissä vaaditaan kaikkien "Tekniset ja prosessiin liittyvät vaatimukset"-kohdassa mainittujen asioiden noudattamista

Tarkemmat sprinteittäiset arvosteluperusteet [täällä](https://github.com/mluukkai/ohtu2016/wiki/Miniprojektin-arvosteluperusteita#sprintin-1-arvosteluperusteet)