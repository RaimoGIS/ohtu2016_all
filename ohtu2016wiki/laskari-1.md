## Huom: ohjausta tehtävien tekoon to 14-17 B221

### Tehtävien palautuksen deadline su 20.3. klo 23.59

## Miten viikon 1 tehtävät palautetaan?

GitHubin ja lopussa olevan "palautuslomakkeen" avulla! Eli teet kaiken mitä alla sanotaan, se riittää, kenellekään ei tarvitse näyttää mitään.

Tämän viikon tehtävät ovat lähes luennoista riippumattomia, eli luentomateriaalista ei ole tehtäviin apua. Kaikki tarvittava materiaali on linkitetty tehtävien yhteyteen.

## 1 Githubiin

Tee itsellesi tarvittaessa tunnus GitHubiin

* mene osoitteeseen https://github.com/plans
* valitse create free account

Luo repositorio nimellä ohtu-viikko1 

* klikkaa yläpalkin oikeassa reunassa olevaa  "Create a new repo"-ikonia 
* **laita rasti** kohtaan Initialize this repository with a README 

![README-tiedoston alustusrasti](https://github.com/mluukkai/ohtu2015/raw/master/images/viikko1-1.png)

Jos haluat käyttöösi yksityisiä repositorioita, voit hakea akateemista tunnusta osoitteesta [https://github.com/edu](https://github.com/edu) kurssin tehtävät voit tehdä myös julkiseen repositorioon.

Luo paikalliselle koneellesi ssh-avain (tapahtuu komentoriviltä käsin)

* Ohje avaimen luomiseen esim. sivulla: [http://www.cs.helsinki.fi/group/kuje/compfac/ssh_avain.html](http://www.cs.helsinki.fi/group/kuje/compfac/ssh_avain.html) kohdassa: Avainparin luonti.

Lisää avaimen julkinen pari githubiin:

* https://github.com/settings/ssh

Näin pystyt käyttämään GitHubia ilman salasanan syöttämistä koneelta, josta juuri luodun avaimen salainen pari löytyy

Konfiguroi nimesi ja email-osoitteesi paikallisen koneesi git:iin antamalla komennot:

    git config --global user.name "Your Name"
    git config --global user.email my.address@gmail.com


Kloonaa nyt githubiin tehty repositorio **paikalliselle koneelle**. Tämä tapahtuu antamalla komentoriviltä komento

    git clone git@github.com:omatunnustahan/ohtu-viikko1.git

missä komennon <code>git clone</code> parametrina on repositoriosi sivulta selviävä 'clone URL' (huomaa, että formaatin on oltava SSH):

![ssh-kloonausurlin sijainti](https://github.com/mluukkai/ohtu2015/raw/master/images/viikko1-2.png)

Nyt paikalliselle koneellesi syntynyt hakemisto <code>ohtu-viikko1</code>, joka on on githubissa olevan repositorion klooni.

## 2 Gitin alkeet

* Lue https://we.riseup.net/debian/git-development-howto ja http://www.ralfebert.de/tutorials/git/, molemmat kohtaan *Branching* asti ja suorita samalla komentoriviltä kaikki dokumentin esimerkit. Git on jo asennettu koulun koneille, joten kohdan *Install git* voit skipata
  * Lisää git-ohjeita esim. [Pro Git -oppaassa](http://git-scm.com/book), kannattaa lukea näin alkuun luku 2
  * Hyviä ohjeita löydät myös [Githubin helpistä](https://help.github.com/articles/)
  * Varsin lupaavalta Git-tutorialilta näyttää myös [https://www.atlassian.com/git/tutorial](https://www.atlassian.com/git/tutorial)
* git saattaa vaikuttaa aluksi sekavalta, pienen totuttelun jälkeen peruskäyttö on kuitenkin helppoa ja se nostaa elämäsi laatua merkittävästi

**tee seuraavat:**

* mene edellisessä tehtävässä luotuun repositorion klooniin (eli komennon <code>git clone</code> luomaan hakemistoon)
* lisää ja committaa repositorioon kaksi tiedostoa ja hakemistoa, joiden sisällä on tiedostoja
  * muista hyödyllinen komento git status
* muuta ainakin kahden tiedoston sisältöä ja committaa muutokset repositorioon
* tee .gitignore-tiedosto, jossa määrittelet, että repositorion juurihakemistossa olevat tiedostot, joiden pääte on _tmp_ ja hakemisto jonka nimi on _target_ ignoroidaan
* lisää tmp-päätteisiä tiedostoja repositorioon ja varmista että git jättää ne huomioimatta
* lisää myös hakemisto nimeltä _target_ ja hakemiston sisälle joku tiedosto. Varmista että target sisältöineen ei mene versionhallinnan alaisuuteen
* tee muutos tiedostoon. Älä lisää tiedostoa "staging"-alueelle
  * peru muutos (git status -komento antaa vihjeen miten tämä tapahtuu)
* tee muutos ja lisää tiedosto "staging"-alueelle
  * peru muutos (git status -komento antaa vihjeen miten tämä tapahtuu)
* tutoriaaleissa ei valitettavasti käytetä git add -komennon hyödyllistä muotoa <code>git add -p</code> 
 * tee muutoksia muutamiin tiedostoihin ja lisää muutokset staging-alueelle komennon git add -p avulla
 * jos lisäät projektiin uusia tiedostoja, ei git add -p huomaa niitä, eli ne on lisättävä staging-alueelle erikseen 
 * käytä jatkossa komentoa git add -p aina kun se on suinkin mahdollista!

## 3 Tiedostojen lisääminen GitHubiin

Tehtävässä 1 tehtiin GitHubiin repostorio, joka liitettiin paikalliselle koneelle luotuun repositorioon "remote repositoryksi". Synkronoidaan paikallisen repositorion ja githubin tilanne:

* "pushaa" nämä GitHubissa olevaan etärepositorioon antamalla komento <code>git push</code>
* varmista selaimella, että lisätyt tiedostot menevät GitHubiin

## 4 Monta kloonia samasta repositoriosta

Yleensä on tapana pitää GitHubissa olevaa repositorioa tiedostojen "keskitettynä" sijoituspaikkana ja liittää paikallisella koneella oleva repositorio GitHubissa olevan repositorion etärepositorioksi, kuten teimme tehtävässä 1. 

Jos työskennellään useammalta koneelta, on githubissa olevasta repositoriosta monta kloonia ja kloonien tila on pidettävä ajantasalla.

Luodaan nyt paikalliselle koneelle repositoriosta toinen klooni:

* mene komentoriville ja esim. kotihakemistoosi  (tai johonkin paikkaan, joka ei ole git-repositorio) 
* anna komento `git clone git@github.com:githubtunnus/repositorionNimi.git nimiKloonille`
  * githubtunnus ja repositorionNimi selviävät  GitHubista repositoriosi sivulta yllä olevan kuvan osoittamasta paikasta
  * *nimiKloonille* tulee olemaan kloonatun repositorion nimi, varmista että annat nimen, jonka nimistä tiedostoa tai hakemistoa ei jo ole kansiossa
* mene kloonattuun repositorioon ja lisää sinne jotain tiedostoja. Committaa lopuksi
* "pushaa" muutokset GitHubiin
* varmista selaimella, että lisätyt tiedostot menevät GitHubiin

**Mene nyt tehtävässä 1 tehtyyn GitHub-repositorion klooniin.**

* alkuperäinen paikallinen klooni ei ole enää ajantasalla, "pullaa" sinne muutokset komennolla <code>git pull</code>
* varmista että molempien paikallisten repositorioiden sisältö on nyt sama
* lisää alkuperäiseen kopioon joitain tiedostoja ja pushaa ne GitHubiin
* mene jälleen kloonattuun kopioon ja pullaa

* hae osoitteesta https://github.com/mluukkai/ohtu2016/blob/master/viikko1/OhtuVarasto.zip?raw=true löytyvä zipattu paketti, pura se kloonattuun repositorioon siten, että **paketissa olevat tiedostot ja hakemistot tulevat repositorion juureen**
* lisää ja committoi zipistä puretut tavarat
* katso vielä kerran selaimella, että GitHubissa kaikki on ajan tasalla

**Huomaa, että repositoriosi tulee näyttää tehtävän jälkeen suunnilleen seuraavalta** (poislukien tehtävissä lisätyt muut tiedostot ja hakemistot):

![githubissa nyt hakemisto src, tiedostot .gitignore, pom.xml, README.md sekä tehtävissä lisätyt tiedostot ja hakemistot](https://raw.githubusercontent.com/mluukkai/ohtu2016/master/images/kuva1-3.png)


Nyt voit poistaa toisen paikallisista kopioista.

## 5 Maven

* lue ensin https://www.ibm.com/developerworks/java/tutorials/j-mavenv2/ kohtaan _working with multiple projects_ asti
  * myös tämä voi olla hyödyksi http://docs.codehaus.org/display/MAVENUSER/The+Maven+2+tutorial  
  * vielä syventävämpää tietoa http://www.sonatype.com/books/mvnref-book/reference/

* edellisessä tehtävässä lisättiin repositorioon maven-muodossa oleva projekti, joka sisältää aikoinaan [ohjelmoinnin perusteissa](http://www.cs.helsinki.fi/u/wikla/ohjelmointi/materiaali/02_oliot/#15) olleen luokan <code>Varasto</code>, sen käyttöä demonstroivan pääohjelman ja muutaman JUnit-testin
* tutki maven-muotoisen projektin hakemistorakennetta esim. antamalla komento <code>tree</code> projektin sisältävän hakemiston juuressa (tree ei ole maveniin liittyvä käsky vaan normaali shell-komento)
  * HUOM: macissa ei ole oletusarvoisesti tree-komentoa
  * mikäli koneellasi on [HomeBrew](http://mxcl.github.com/homebrew/) asennettuna, saat tree:n asennettua <code>brew install tree</code>
  * vaihtoehtoisesti saat treetä vastaavan toiminnallisuuden macissa komennolla <code>find .  ! -regex './\..'  -print | sed -e 's;[^/]*/;|__;g;s;__|; |;g'</code>
  * myöskään kaikissa linuxeissa ei ole tree oletusarvoisesti asennettu. debian-pohjaisissa linuxeissa (esim ubuntussa) saat asennettua tree:n komennolla <code>sudo apt-get install tree</code>
* tarkastele projektin määrittelevän pom.xml-tiedoston sisältöä

**avaa edellisen tehtävän projekti suosikki-idelläsi**
 
*  NetBeans ja IntellijIdea tukevat maven-muotoisia projekteja suoraan
* jos NetBeans ei tunnista projektia, asenna maven-plugin valitsemalla tools -> plugins -> available plugins
* HUOM: jos et ole aiemmin kääntänyt koneellasi maven-muotoisia projekteja, saattaa IDE valittaa tässä vaiheessa joidenkin kirjastojen (mm. JUnit) puuttumisesta. Asia korjaantuu "buildaamalla" tai kääntämällä projekti komentoriviltä (ohjeet alla) 
* Ohjeita Eclipse-käyttäjille http://maven.apache.org/eclipse-plugin.html  

Ohjelmakoodin editointi kannattaa tehdä IDE:llä ja välillä myös ohjelman ja testien ajaminenkin, mutta **mavenia kannattaa kuitenkin ehdottomasti totutella käyttämään myös komentoriviltä.** 

**Kokeile seuraavia:**
 
* aloita puhtaalta pöydältä: <code>mvn clean</code>
* tee juuressa komento <code>tree</code>
* käännä: <code>mvn compile</code>
  * tee jälleen juuressa komento tree. Mitä muutoksia huomaat?
  * huom: projekti olettaa, että koneellasi on javan jdk:sta vähintään versio 1.7. Jos koneellasi on vanhempi versio, asenna uudempi jdk
* käännöksen jälkeen voit suorittaa pääohjelman komennolla <code>mvn exec:java -Dexec.mainClass=ohtu.ohtuvarasto.Main</code>
  * parametrina siis main-metodin sisältävän luokan nimi
* tee <code>mvn clean</code> ja yritä suorittaa ohjelma uudelleen. Miten käy?
  * suorita cleanin jälkeen tree-komento. Mitä clean tekee?
* aja testit komennolla <code>mvn test</code>
  * suorita jälleen komento <code>tree</code>
  * huomaat, että testien ajaminen luo hakemiston target/surefire-reports. Testien diagnostiikka tulee tähän hakemistoon
* tee projektista [jar](https://en.wikipedia.org/wiki/JAR_(file_format))-tiedosto: <code>mvn install</code>
  *  komennolla tree näet minne hakemistoon jar tulee
* suorita jar komennolla <code>java -cp tiedostonNimi.jar ohtu.ohtuvarasto.Main</code>
  * komento siis annetaan hakemistosta, jossa jar-tiedosto sijaitsee 

**HUOM** mavenin versio 3 komennolla <code>mvn install</code> tehty jar ei välttämättä toimi. Jos näin käy, tee seuraavasti:

* lisää projektissa olevaan _pom.xml_-tiedostoon _plugins_-tägien sisään seuraava:

```html
       <plugins>
            //
            // älä koske olemassaoleviin plugineihin!
            //            

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.2.1</version>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>

       </plugins>
```

* generoi jar-komennolla <code>mvn assembly:assembly</code>

## 6. JUnit

* kertaa JUnitin perusteet osoitteesta [https://github.com/mluukkai/OTM2015/wiki/JUnit-ohje](https://github.com/mluukkai/OTM2015/wiki/JUnit-ohje)
* täydennä tehtävässä 4 repositorioosi lisäämäsi projektin testejä siten, että luokan Varasto rivikattavuudeksi (line coverage) tulee 100%
  * Joudut huomioimaan ainakin tapaukset, joissa varastoon yritetään laittaa liikaa tavaraa ja varastosta yritetään ottaa enemmän kuin siellä on
  * edellinenkään ei vielä riitä
* testauskattavuuden saat selville seuraavasti:
  * käytetään mavenissa pluginina olevaa [cobertura](http://cobertura.sourceforge.net/)-nimistä koodikattavuustyökalua
  * suorita projektin juuresta komento <code>mvn cobertura:cobertura</code>
    * Jos törmäät virheeseen _Could not find artifact com.sun:tools:jar:0 at specified path /System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home/../lib/tools.jar_, asenna Java jdk 7 ja määrittele <code>JAVA_PATH</code>-ympäristömuuttuja. Ohje ympäristömuuttujan asettamiseen [OSX:llä](http://www.mkyong.com/java/how-to-set-java_home-environment-variable-on-mac-os-x/)
  * kattavuusraportit tulevat hakemistoon target/site/cobertura
  * saat avattua raportin esim. komennolla <code>firefox target/site/cobertura/index.html</code>
* kun luokan <code>Varasto</code> testien rivikattavuus (line coverage) on 100%, pushaa tekemäsi muutokset GitHubiin

## 7. TravisCI, osa 1

[https://travis-ci.org/](https://travis-ci.org/) on jatkuvaan integrointiin ja käyttöönottoon tarkoitettu web-palvelu. 

Konfiguroidaan seuraavaksi Travis huolehtimaan projektistamme. 

* mene osoitteeseen [https://travis-ci.org/](https://travis-ci.org/) ja valitse _sign in with GitHub_
* klikkaa omaa nimeäsi. Valitse _accounts_
* näin avautuu lista GitHub-repositorioistasi
![kuvassa lista githubissa olevia repositorioita joissa jokaisen kohdalla "valintakytkin"](https://raw.githubusercontent.com/mluukkai/ohtu2016/master/images/kuva1-4.png)
* jos et näe listalla edellisissä tehtävissä luotua repositoria, paina _Sync account_
* paina repositorion kohdalla olevaa valitsinta siten, että se muuttuu vihreäksi

Tämän jälkeen Travis alkaa tarkkailla jokaista muutosta, jonka teet repositorioon.

* Tee repositorioon joku muutos ja pushaa se GitHubiin. 
* Mene osoitteeseen [https://travis-ci.org/](https://travis-ci.org/)
* hetken kuluttua sivulle avautuu näkymä, joka kertoo siitä, että Travis yrittää _buildata_ koodin, jonka repositorio sisältää

![lokinäkymä, joka näyttää miten projektin buildaus etenee travisissa](https://raw.githubusercontent.com/mluukkai/ohtu2016/master/images/kuva1-5.png)

## 8. TravisCI, osa 2

Edellinen tehtävässä tekemämme Travis-build päättyy seuraavaan virheilmoitukseen
<pre>
WARNING: We were unable to find a .travis.yml file. This may not be what you
  want. Build will be run with default settings.
</pre>

Travis ei siis löydä projektista konfiguraatiotiedostoa <code>.travis.yml</code>.

* Lisää projektiin tiedosto .travis.yml, jonka sisältö on seuraava:
```yml
language: java
```
* Pushaa muutos githubiin ja tarkkaile projektin Travis-näkymää. Näkymän päivittymiseen menee hetki. Uusi buildi saattaa nääkyä jo hieman aiemmin, jos menet välilehdelle _braches_ tai _build history_
* Jos testisi menevät läpi paikallisesti, pitäisi seurauksena olla onnistunut Travis-build

Mitä Travisissa oikeastaan tapahtuu?

* Kun Travisiin rekisteröity projekti pushataan GitHubiin, ilmoittaa GitHub-asiasta Travisille
* Travis käynnistää uuden virtuaalikoneen, jolle se kloonaa muuttuneen repositorion (komennolla git clone ...)
* Travis lukee repositoriossa olevan konfiguraatiotiedoston <code>.travis.yml</code> ja toimii tiedostossa olevien ohjeiden mukaan
* Jos kyseesä on Java-projekti, suorittaa Travis oletusarvoisesti [projektille](https://docs.travis-ci.com/user/languages/java) komennot <code>mvn install</code ja <code>mvn test</code> eli suorittaa projektin testit. Jos testit menevät läpi on Travis buildin tila _passed_.

Travis-buildien toimintaa on mahdollista [konfuguroida](https://docs.travis-ci.com/user/customizing-the-build/) melko vapaasti.

* Muuta jostain testiä siten, että testi ei mene läpi ja pushaa koodi GitHubiin
* Tarkkaile projektin Travis-näkymää. Lue Travisnäkymään tulostuva loki kokonaisuudessaan läpi

* Korjaa testi ja pushaa muutokset uudelleen GitHubiin
* Tarkkaile jälleen Travis-näkymää ja lue loki läpi

## 9. TravisCI, osa 3
Laita repositoriossasi olevaan tiedostoon _README.md_ koodin tilasta kertova [Travis Badge](https://docs.travis-ci.com/user/status-images/)

Tee nyt jokin muutos repositorioosi ja yritä pushata koodi GitHubiin. Toimenpiteestä seuraa virhe:

<pre>
To git@github.com:mluukkai/ohtutesti16.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'git@github.com:mluukkai/ohtutesti16.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
</pre>

Tulet todennäköisesti törmäämään vastaavaan virheeseen usein. Syynä virheelle on se, että yrität pushata muutoksia GitHubiin vaikka GitHub on "edellä" paikallista repositorioasi (ts. sinne lisättiin tiedosto README.md).

Ongelma ratkeaa, kun teet ensin komennon <code>git pull</code> ja pushaat koodin vasta sen jälkeen. 

## 10. Coveralls

Tehtävässä 6 määrittelimme projektin testauskattavuuden _coberturan_ avulla. [coveralls.io](https://coveralls.io/)-palvelu mahdollistaa projektien koodikattavuuden julkaisemisen verkossa. 

* kirjaudu [Coveralssiin](https://coveralls.io/) (GitHub signin)
* jos repositorio ei näy avautuvalla listalla, klikkaa "re-sync repos"
* lisää repositorio Coverallsin alaisuuteen

Projektille on määritelty valmiiksi Coverallsin [Java](https://github.com/trautonen/coveralls-maven-plugin)-plugin

Saamme generoitua projektimme testiraportin lisäämällä tiedostoon <code>.travis.yml</code> seuraavat rivit:

```yml
after_success:
  - mvn clean cobertura:cobertura coveralls:report
```

Käytännössä pyydämme Travisia suorittamaan onnistuneen buildin (eli komennon <code>mvn test</code>) jälkeen maven-komennon, joka ensin suorittaa testien kattavuusanalyysin coberturalla ja sen jälkeen lähettää tiedot coverallsiin

Pushaa muutokset GitHubiin ja seuraa _sekä_ Travis-buildin lokia, että repositorion coveralls-sivua (jonne pääset repositorion vierestä olevasta _details_-napista)

Lisää projektin _readme badge_ (ks. projektin coveralls-sivun oikea laita) repositoriosi README.md-tiedostoon.

Projektisi GitHub-sivun tulisi lopulta näyttää suunilleen seuraavalta:

![badget githubin readmessa](https://raw.githubusercontent.com/mluukkai/ohtu2016/master/images/kuva1-6.png)

Huomaa, että Travisin ja Coverallsin badget eivät päivity täysin reaaliajassa. Eli vaikka projektin testikattavuus nousisi kestää hetken ennen, kuin badge näyttää tuoreen tilanteen.

## 11. Forkaa repositorio https://github.com/mluukkai/ohtu2016

* forkkaaminen tapahtuu seuraavasti:
  * kun olet kirjautuneena GitHubiin, mene yo. osoitteeseen
  * paina oikeassa yläkulmassa olevaa nappia "fork"
* saat näin oman "forkatun" kopion repositoriosta ohtu2016
* kloonaa forkattu repositorio paikalliselle koneellesi
* lisää repositorioon hakemisto, jonka nimi on muotoa SukunimiEtunimi
  * eli esim. oma hakemistoni olisi LuukkainenMatti
* lisää hakemiston sisälle tiedosto nimeltä viikko1
* pushaa muutokset githubiin
* tee forkkaamastasi repositoriosta "pull request"
  * mene selaimella forkkaamaasi repositorioon
  * paina oikeassa kulmassa olevaa nappia "pull request"
  * anna tehtävistä palautetta avautuvaan lomakkeeseen

## tehtävien kirjaaminen palautetuksi

* Kirjaa tekemäsi tehtävät [tänne](http://ohtustats2016.herokuapp.com) 
  * huom: tehtävien palautuksen deadline on su 20.3. klo 23.59