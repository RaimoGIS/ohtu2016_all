<pre>
Tehtävien palautuksen deadline su 10.4. klo 23.59

Ohjausta tehtävien tekoon to 7.4. 14-17 B221
</pre>

## palautetaan GitHubin kautta

* **tämän viikon palautusta varten tehtävä repositorio tehdään tehtävässä 6**
* palautusrepositorion nimi ilmoitetaan tehtävien lopussa olevalla palautuslomakkeella

## 1. lisää mavenia: koodin staattinen analyysi

* luennolla 5 puhuttiin koodikatselmoinnin yhteydessä staattisen analyysin työkaluista, joita voidaan käyttää koodin katselmoinnin apuna
* tutustu staattisen analyysin työkaluun chekstyleen ks. [http://checkstyle.sourceforge.net/](http://checkstyle.sourceforge.net/)
* checkstyleä on helppo käyttää maven-projekteissa, sillä checkstyle on valmiiksi konfiguroituna pluginina mavenissa, ks. [http://maven.apache.org/plugins/maven-checkstyle-plugin/](http://maven.apache.org/plugins/maven-checkstyle-plugin/) checkstyleä kannattaa käyttää yhdessä [http://maven.apache.org/plugins/maven-jxr-plugin/](http://maven.apache.org/plugins/maven-jxr-plugin/):n kanssa
  * jxr:n avulla checkstylen raportista pääsee klikkaamalla vastaaville sorsakoodin riveille

mene nyt johonkin valmiiseen projektiisi, esim. viikon 2 verkkokauppaan
* kokeile suorittaa <code>mvn jxr:jxr checkstyle:checkstyle</code>
* avaa raportti selaimella polulta __/target/site/checkstyle.html__
  * tutki raporttia
* oletusarvoisesti raportoidaan paljon kaikenlaista ja oleellinen uhkaa hukkua detaljien joukkoon

checkstylen tarkkailemien virheiden joukko on konfiguroitavissa erillisen koniguraatiotiedoston avulla
* hae repositiorion [https://github.com/mluukkai/ohtu2016/](https://github.com/mluukkai/ohtu2016/) hakemistosta viikko3 konfiguraatiotiedoston pohja __my_checks.xml__, talleta se sopivaan paikkaan, kokeile:
        <code>mvn jxr:jxr checkstyle:checkstyle -Dcheckstyle.config.location=my_checks.xml</code>       
* komennossa oleva polku olettaa että konfiguraatiotiedosto sijaitsee projektihakemiston juuressa
  * saattaa olla, että windowsissa komentoon on lisättävä pari hipsua: <code>mvn jxr:jxr checkstyle:checkstyle -D"checkstyle.config.location=my_checks.xml"</code>  
  * huom: saattaa olla, että komento ei toimi windowsilla, ongelma ehkä vaan win 8:ssa. jos näin käy, konfiguraatiotiedoston sijainnin voi määritellä pom.xml-tiedostossa seuraavasti:


```XML
<plugins>
  <!-- muut mahdolliset pluginit -->
 
  <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-checkstyle-plugin</artifactId>
    <version>2.10</version>
    <configuration>
      <configLocation>my_checks.xml</configLocation>
    </configuration>
  </plugin>
</plugins>
```

**Toimi nyt seuraavasti**

* tee alkuperäisestä konfiguraatiotiedostosta kopio, ja poista kopiosta kaikki elementin <code>tree walker</code> sisällä olevat tarkistukset 
* määrittele tiedostoon seuraavat säännöt (ks. available checks ja standard checks checkstylen [sivuilta](http://checkstyle.sourceforge.net/)):
  * metodien pituus max 10 riviä (tämä ja seuraavat säännöt määritellään moduulin tree walker sisälle)
  * ei yli yhtä sisäkkäisiä if:iä
  * ei sisäkkäisiä for:eja
  * koodi on oikein sisennettyä
  * lohkon avaava aaltosulku on aina rivin lopussa
  * syklomaattinen koodikompleksisuus korkeinaan 3 (selvitä mitä tarkoittaa!)
  * tee koodiin muutoksia, jolla testaat että rikkoutuvat ehdot huomataan

* kun suoritamme tarkastukset komennolla <code>mvn checkstyle:checkstyle ...</code> checkstyle ainoastaan generoi raportin mahdollisista sääntörikkeistä. jos tarkastukset suoritetaan komennolla <code>mvn checkstyle:check ...</code>, aiheuttaa sääntörikkomus sen, että projektin käännös ei onnistu.
* muuta koodiasi siten, että jokin checkstyle-sääntö rikkoutuu ja suorita checkstyle komennolla
<code>mvn checkstyle:check -D"checkstyle.config.location=my_checks.xml"</code>  
* korjaa koodisi ja suorita edellinen komento uudelleen

Jos projektissa on käytössä travis.ci, kannattaakin käyttää komentoa <code>mvn checkstyle:check ...</code> jotta tyylirikkeet aiheuttavat travis-buildin rikkoutumisen


## 2. tutustuminen easyB:hen

Lue seuraava [https://github.com/mluukkai/ohtu2016/blob/master/web/easyb.md](https://github.com/mluukkai/ohtu2016/blob/master/web/easyb.md)

tutustu linkin takana olevan ohjelman rakenteeseen ja aja siihen liittyvään testit.
* käynnistä ohjelma <code>mvn exec ...</code> -komennolla (ks. [viikon 1 laskarit](Laskari-1#5-maven))
* ohjelman tuntemat komennot ovat __login__ ja __new__

## 3. Kirjautumisen testit

tee User storyn *User can log in with valid username/password-combination* kaikista testeistä ajettavia

**HUOM:** jos testit eivät mene läpi **lue koko virheilmoitus**, ongelmasta kertovat oleelliset asiat löytyvät virheilmoituksen seasta.

## 4. Uuden käyttäjän rekisteröitymisen testit

tee User storyn *A new user account can be created if a proper unused username and a proper password are given* kaikista testeistä ajattavia.

* käyttäjätunnuksen on oltava merkeistä a-z koostuva vähintään 3 merkin pituinen merkkijono, joka ei ole vielä käytössä
* salasanan on oltava pituudeltaan vähintään 8 merkkiä ja sen tulee sisältää vähintään yksi numero tai erikoismerkki
* **Täydennä ohjelmaa siten että testit menevät läpi**

Testejä kannattaa tehdä yksi kerrallaan, laittaen samalla vastaava ominaisuus ohjelmasta kuntoon. Jos testit eivät mene läpi **lue koko virheilmoitus**, ongelmasta kertovat oleelliset asiat löytyvät virheilmoituksen seasta.

**HUOM** voit vähentää testikoodisi copypastea määrittelemällä testeille apumetodeja. Ohjeita metodien määrittelemiseen ym. [täällä](http://groovy-lang.org/documentation.html) tai googlella

## 5. Spring jälleen kerran

Ennen kuin sovellus päästään käynnistämään, on se konfiguroitava:

``` java
public static void main(String[] args) {
    UserDao dao = new InMemoryUserDao();
    IO io = new ConsoleIO();
    AuthenticationService auth = new AuthenticationService(dao);
    new App(io, auth).run();
}
```

Muuta ohjelmaa siten, että sovelluksen konfigurointi hoidetaan Springin avulla (joko xml- tai annotaatioperustaisesti), ja main:iksi riittää:

``` java
public static void main(String[] args) {
    ApplicationContext ctx = new FileSystemXmlApplicationContext("src/main/resources/spring-context.xml");
 
    App application = ctx.getBean(App.class);
    application.run();
}
```

Ohjeita löytyy viikon 2 laskareiden [lisämateriaalista](https://github.com/mluukkai/ohtu2016/blob/master/web/riippuvuuksien_injektointi.md#dependency-injection-spring-sovelluskehyksessä)

## 6. Travisiin

Laitetaan edellisen tehtävän projekti Travisin tarkkailtavaksi.

* tee projektihakemistosta github-repositorio 
  * **HUOM:** älä laita repositorioa minkään muun repositorion sisälle, siitä seuraa ongelmia
  * varmista että koodi, eli tiedosto _pom.xml_ ja hakemisto _src_ ovat **repositorion juuressa**, jos näin ei ole, on seurauksena ongelmia
* kopioi tehtävässä 1 tehty checkstyle-konfiguraatiotiedosto repositorion juureen
* suorita koodille checkstyle-tarkastus 
  * refaktoroi koodia siten, että ehdot täyttyvät
* seuraa viikon 1 ohjeita Travisin suhteen
* Huomaa, että [oletusarvoisesti](https://docs.travis-ci.com/user/languages/java) Travis suorittaa maven-projekteille komennon <code>mvn test</code>. Kuten edellisessä tehtävässä näimme, projektimme testit suoritetaan komennolla <code>mvn integration-test</code>
* konfiguroi Travis suorittamaan projektille testit _sekä_ sellainen checkstyle-tarkastus, joka hajottaa buildin jos koodi rikkoo checkstylen ehtoja
  * katso [https://docs.travis-ci.com/user/customizing-the-build/](https://docs.travis-ci.com/user/customizing-the-build/) kohta _Customizing the Build Step_ ja tehtävä 1
* testaa että Travis-buildi hajoaa kun muutat koodia siten, että
  * jokin testi ei mene läpi
  * koodi rikkoo jonkin checkstyle-ehdon

## 7. Retrospektiivitekniikat

Wikipedian mukaan retrospektiivi on _"a meeting held by a project team at the end of a project or process (often after an iteration) to discuss what was successful about the project or time period covered by that retrospective, what could be improved, and how to incorporate the successes and improvements in future iterations or projects."_

Tutstu sivulla [http://retrospectivewiki.org/index.php?title=Retrospective_Plans](http://retrospectivewiki.org/index.php?title=Retrospective_Plans) esiteltyihin retrospektiivitekniikoihin [Start, Stop, Continue, More of, Less of Wheel](http://retrospectivewiki.org/index.php?title=Start,_Stop,_Continue,_More_of,_Less_of_Wheel) ja [Glad, Sad, Mad](http://retrospectivewiki.org/index.php?title=Glad,_Sad,_Mad)

Tee aiheesta noin 0.25 sivun tiivistelmä repositorion juureen sijoitettavaan tiedostoon _retro.md_

Pidä huoli siitä, että miniprojektitiimisi pitää ensimmäisen sprintin lopussa jompaa kumpaa tekniikkaa noudattavan retrospektiivin!

## 8. FileUserDAO

Tallenna ohjelma käyttäjätiedot tiedostoon. Hoida asia siten, että teet luokan <code>FileUserDAO</code>, joka toteuttaa rajapinnan <code>UserDAO</code> ja sisältää kaiken tiedostonkäsittelyyn liittyvän koodin. Anna FileUserDAO:lle sen käyttämän tiedoston nimi konstruktorin parametrina. Testatessa on edelleen mielekästä käyttää InMemoryUserDAO:a.

* Jos tiedostojen käsittely on päässyt unohtumaan, ohjeita esim. [Ohjelmoinnin jatkokurssin](https://www.cs.helsinki.fi/group/java/s15-materiaali/) viikoilta [10](https://www.cs.helsinki.fi/group/java/s15-materiaali/viikko10/) ja [12](https://www.cs.helsinki.fi/group/java/s15-materiaali/viikko12/)
  * jos salasanatiedosto sijaitsee projektihakemiston juuressa, sen luettavaksi avaaminen onnistuu komennolla <code>new Scanner(new File("salasanat.txt"));</code>
  * päätä itse mitä tapahtuu tilanteessa, jossa parametrina annettua tiedostoa ei ole olemassa

Jos teit tehtävän 5, muokkaa Spring-konfiguraatiosi ottamaan huomioon uusi tilanne. Huom: joutunet konfiguroimaan FileUserDAO:n xml:ssä, sillä merkkijonomuotoista konstruktoriparametria ei pysty injektoimaan @Autowired-annotaatiolla. Ohje String-tyyppisen arvon injektointiin xml-konfiguraatiossa [täällä](http://www.roseindia.net/tutorial/spring/spring3/ioc/springconstructorargs.html)

## tehtävien kirjaaminen palautetuksi

tehtävien kirjaus:

* Kirjaa tekemäsi tehtävät [tänne](http://ohtustats2016.herokuapp.com) 
  * huom: tehtävien palautuksen deadline on su 10.4. klo 23.59

palaute tehtävistä:

* Lisää viikon 1 tehtävässä 11 forkaamasi repositorion omalla nimelläsi olevaan hakemistoon tiedosto nimeltä viikko3
* tee viime viikon tehtävän tapaan pull-request
  * anna tehtävistä palautetta avautuvaan lomakkeeseen
  * huom: jos teet tehtävät alkuviikosta, voi olla, että edellistä pull-requestiasi ei ole vielä ehditty hyväksyä ja et pääse vielä tekemään uutta requestia