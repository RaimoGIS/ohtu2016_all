Tohtoriopiskelija Arto Vihavainen tarvitsee järjestelmän, joka auttaa häntä hallitsemaan väitöskirjatyössä tarpeellisia lähdeviitteitä.

Arto kirjoittaa väitöskirjan ja tutkimustaan raportoivat artikkelin LaTeX:illa.

Seuraavassa Arton  tyylinäyte:

![kuva latexilla tehdystä artikkelista](http://www.cs.helsinki.fi/u/mluukkai/viitteet.png)

Huomaamme että artikkeli päättyy oikeaoppisesti lähdeviitteisiin ja juuri lähdeviitteiden hallintaan nyt toteutettavan ohjelmiston on tarkoitus tuoda helpotusta.

LaTeX-dokumenteissa lähdeviitteet kirjoitetaan ns. BibTex-muodossa. Ote edellisen artikkelin lähdeviitteistä:

<pre>
@inproceedings{VPL11,
author = {Vihavainen, Arto and Paksula, Matti and Luukkainen, Matti},
title = {Extreme Apprenticeship Method in Teaching Programming for Beginners.},
year = {2011},
booktitle = {SIGCSE '11: Proceedings of the 42nd SIGCSE technical symposium on Computer science education},
}
 
@book{BA04,
author = {Beck, Kent and Andres, Cynthia},
title = {Extreme Programming Explained: Embrace Change (2nd Edition)},
year = {2004},
publisher = {Addison-Wesley Professional},
}
 
@book{Martin09,
author = {Martin, Robert},
title = {Clean Code: A Handbook of Agile Software Craftsmanship},
year = {2008},
publisher = {Prentice Hall},
}
 
@inproceedings{PSMM07,
author = {Pears, Arnold and Seidman, Stephen and Malmi, Lauri and Mannila, Linda and Adams, Elizabeth and Bennedsen, Jens and Devlin, Marie and Paterson, James},
title = {A survey of literature on the teaching of introductory programming},
booktitle = {ITiCSE-WGR '07: Working group reports on ITiCSE on Innovation and technology in computer science education},
year = {2007},
pages = {204--223},
publisher = {ACM},
}
 
@inproceedings{GvG00,
author = {Grissom, Scott and Van Gorp, Mark J.},
title = {A practical approach to integrating active and collaborative learning into the introductory computer science curriculum},
booktitle = {Proceedings of the seventh annual consortium on Computing in small colleges midwestern conference},
year = {2000},
pages = {95--100},
publisher = {Consortium for Computing Sciences in Colleges},
address = {USA},
}
 
 
@article{W04,
author = {Whittington, Keith J.},
title = {Infusing active learning into introductory programming courses},
journal = {J. Comput. Small Coll.},
volume = {19},
number = {5},
year = {2004},
pages = {249--259},
publisher = {Consortium for Computing Sciences in Colleges},
address = {USA},
}
 
@BOOK (V78,
AUTHOR = "L. S. Vygotsky",
TITLE= "Mind in Society: The Development of Higher Psychological Processes",
PUBLISHER = "Harvard University Press",
YEAR = 1978,
ADDRESS = "Cambridge, MA",
)
 
@inproceedings{AR95,
author = {Astrachan, Owen and Reed, David},
title = {{A}{A}{A} and {C}{S} 1: the applied apprenticeship approach to {C}{S} 1},
booktitle = {SIGCSE '95: Proceedings of the twenty-sixth SIGCSE technical symposium on Computer science education},
year = {1995},
pages = {1--5},
publisher = {ACM},
}
 
@ARTICLE{CBH91,
    author = {Allan Collins and John Seely Brown and Ann Holum},
    title = {Cognitive apprenticeship: making thinking visible},
    journal = {American Educator},
    year = {1991},
    volume = {6},
    pages = {38--46}
}
 
@ARTICLE{RRR03,
    author = {Anthony Robins and Janet Rountree and Nathan Rountree},
    title = {Learning and teaching programming: A review and discussion},
    journal = {Computer Science Education},
    year = {2003},
    volume = {13},
    pages = {137--172}
}
 
@inproceedings{KB04,
author = {K\"{o}lling, Michael and Barnes, David J.},
title = {Enhancing apprentice-based learning of Java},
booktitle = {SIGCSE '04: Proceedings of the 35th SIGCSE technical symposium on Computer science education},
year = {2004},
pages = {286--290},
publisher = {ACM},
}
 
@article{B06,
author = {Black, Toni R.},
title = {Helping novice programming students succeed},
journal = {J. Comput. Small Coll.},
volume = {22},
number = {2},
year = {2006},
pages = {109--114},
publisher = {Consortium for Computing Sciences in Colleges},
address = {USA},
}
 
 
@inproceedings{CB07,
author = {Caspersen, Michael E. and Bennedsen, Jens},
title = {Instructional design of a programming course: a learning theoretic approach},
booktitle = {ICER '07: Proceedings of the third international workshop on Computing education research},
year = {2007},
pages = {111--122},
publisher = {ACM},
}
 
@article{BB03,
author = {Bruhn, Russel E. and Burton, Philip J.},
title = {An approach to teaching Java using computers},
journal = {SIGCSE Bull.},
volume = {35},
number = {4},
year = {2003},
pages = {94--99},
publisher = {ACM},
}
 
@inproceedings{R02,
author = {Roumani, Hamzeh},
title = {Design guidelines for the lab component of objects-first CS1},
booktitle = {SIGCSE '02: Proceedings of the 33rd SIGCSE technical symposium on Computer science education},
year = {2002},
pages = {222--226},
publisher = {ACM},
}
 
@article{SS86,
author = {Spohrer, James C. and Soloway, Elliot},
title = {Novice mistakes: are the folk wisdoms correct?},
journal = {Commun. ACM},
volume = {29},
number = {7},
year = {1986},
pages = {624--632},
publisher = {ACM},
}
 
@inproceedings{HM06,
author = {Hassinen, Marko and M\"{a}yr\"{a}, Hannu},
title = {Learning programming by programming: a case study},
booktitle = {Baltic Sea '06: Proceedings of the 6th Baltic Sea conference on Computing education research: Koli Calling 2006},
year = {2006},
pages = {117--119},
publisher = {ACM},
}
</pre>

Lisää bibtexistä esim. seuraavassa:

* <http://en.wikipedia.org/wiki/BibTeX>

Arto haluaa järjestelmän, jonka avulla hän voi hallinnoida viitteitä helposti. Ohjelmalla pitää olla ainakin seuraavat ominaisuudet:

* viitteitä pitää pystyä lisäämään järjestelmään ihmiselle hyvässä muodossa, esimerkiksi jonkun lomakkeen avulla
  * esim. ääkköset pitää toimia kunnolla, ei saa joutua lisäämään hankalassa muodossa kuten yllä, samoin kirjainlyhenteet 
* järjestelmässä olevista viitteistä pitää saada generoitua LaTeX-dokumenttiin sopiva BibTeX-muotoinen tiedosto
* myös viitteiden listaaminen ihmiselle sopivammassa formaatissa pitää onnistua
* viitelistoja pitäisi pystyä jotenkin rajoittamaan
  * esim. kirjottajan, vuoden, julkaisun mukaan
  * olisi kyllä hyvä, jos jokaiseen viitteeseen voisi liittää joukon kategorioita tai tägejä, jotka mahdollistaisivat tarkemmat haut
* ihan jees jos kyseessä on yhdellä koneella toimiva sovellus, parempi olisi kuitenkin jos se olisi verkossa ja joka paikassa käytettävissä
  * jos toimii vaan paikallisella koneella, pitää eri koneiden välillä pystyä jotenkin synkronoimaan talletetut viitteet
* sellanen olisi loistavaa, että jos antaa linkin acm-digitaalikirjastoon, esim. [näin](http://dl.acm.org/citation.cfm?id=2380552.2380613&coll=DL&dl=GUIDE&CFID=293493744&CFTOKEN=23554239), niin softa crawlaa sieltä kaikki viitteet

vaatimuksia tarkennetaan asiakkaan kanssa viikoittaisissa palavereissa