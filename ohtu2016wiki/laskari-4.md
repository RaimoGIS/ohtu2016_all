<pre>
Tehtävien palautuksen deadline su 17.4. klo 23.59

Ohjausta tehtävien tekoon to 14.4. 14-17 B221
</pre>
## palautetaan GitHubin kautta

* palautusta varten voit käyttää samaa repoa kuin esim. viikon 2 tehtävissä
* palautusrepositorion nimi ilmoitetaan tehtävien lopussa olevalla palautuslomakkeella


## 1. lisää mavenia: pom.xml

Maven-projekti konfiguroidaan projektin juuressa olevassa pom.xml-tiedostossa.

Tutkitaan hieman [viime viikon tehtävissä 3-5](https://github.com/mluukkai/ohtu2016/wiki/laskari-3) käytetyn projektin eli repositorion [https://github.com/mluukkai/ohtu2016](https://github.com/mluukkai/ohtu2016) hakemistossa __viikko4/LoginWeb2__ olevan projektin pom.xml:in sisältöä.

```xml

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>LoginEasyBv1</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>LoginEasyBv1</name>
    <url>http://maven.apache.org</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <easyb.version>1.5</easyb.version>
        <cobertura.version>2.4</cobertura.version>
        <org.springframework.version>3.1.1.RELEASE</org.springframework.version>
    </properties>

    <dependencies>
        
        <!-- testing -->
        
        <dependency>
            <groupId>org.easyb</groupId>
            <artifactId>easyb-core</artifactId>
            <version>${easyb.version}</version>
            <scope>test</scope>        
        </dependency>
        
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit-dep</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>
            
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <version>1.1</version>
            <scope>test</scope>
        </dependency>
        
         <!-- SPRING -->
        
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${org.springframework.version}</version>
        </dependency>        
        
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${org.springframework.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <version>${org.springframework.version}</version>
        </dependency>                        

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${org.springframework.version}</version>
        </dependency>   
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.6</source>
                    <target>1.6</target>
                </configuration>
            </plugin>
            
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>${cobertura.version}</version>
                <configuration>
                    <formats>
                        <format>html</format>
                        <format>xml</format>
                    </formats>
                </configuration>
            </plugin>
                                  
            <plugin>
                <groupId>org.easyb</groupId>
                <artifactId>maven-easyb-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>integration-test</phase>
                        <goals>
                            <goal>test</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <storyType>html</storyType>
                    <storyReport>${basedir}/target/easyb/easyb-report.html
                    </storyReport>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

Alussa määritellään projektin tiedot (nimi, versio.)

Kohdassa *properties* määritellään mm. alempana käytettäviä vakioita

Maven osaa ladata riippuvuuksia (eli käytännössä jar-tiedostoja) automaattisesti oletusrepositorioista. Kaikki riippuvuudet eivät kuitenkaan löydy oletusrepositorioista ja tälläisiä tilanteita varten osaan *repositories* voi määritellä vaihtoehtoisia repositorioita joista maven voi etsiä riippuvuuksia.

Riippuvuudet määritellään osassa *dependencies*

* alussa olevien riippuvuuksien (mm. easyb, junit) scope on _test_, tämä tarkoittaa että ne ovat käytössä vain testeissä
* selenium-riippuvuuden scope on _compile_, tällöin selenium on käytössä testeissä ja normaalissa koodissa
* jos ohjelmassa tarvitaan jar:eja, tulee niitä vastaavat maven-riippuvuudet kirjata dependencies-osaan, riippuvuuksia voi etsiä mm. seuraavista: [http://search.maven.org](http://search.maven.org) tai [http://mvnrepository.com/](http://mvnrepository.com/)

Osassa *build* määritellään kääntämiseen liittyvien pluginien toimintaa

* kääntämisessä määritellään käytettävän javan versiota 1.6, tämä tapahtuu maven-compiler-plugin:ia konfiguroimalla
  * jos tätä konfiguraatiota ei tehdä käyttää compiler-plugin oletusarvoista javan versiota. maven 3:ssa se on 1.6 mutta maven 2.*:ssa versio 1.3
* seuraavaksi määritellään, että cobertura-plugin tuottaa raporttinsa html:nä ja xml:nä
* jetty-pluginiin liittyy enemmänkin konfiguraatioita
  * kohdan __executions__-alla määritellään, että jetty (eli sovelluksen käyttämä maven-projektiin integroitu web-palvelin) käynnistetään vaiheessa __pre-integration-test__ ja sammutetaan vaiheessa __post-integration-test__, tämä saa aikaan sen, että kun ajetaan integraatiotestejä, eli suoritetaan komento <code>mvn integration-test</code>, on jetty päällä testien ajamisen aikana
* easyb-pluginin määritellään ajavan testit __integration-test__-vaiheessa

## 2. lisää mavenia: riippuvuuksien lisääminen

Hae repositorion [https://github.com/mluukkai/ohtu2016](https://github.com/mluukkai/ohtu2016) hakemistossa viikko4/TyhjaProjekti lähes tyhjän maven-projektin runko.

* mukana on kohta tarvitsemasi luokka __Submission__ 

Tehdään ohjelma jonka avulla voit lukea kurssilla palauttamiesi tehtävien statistiikan osoitteesta [http://ohtustats2016.herokuapp.com/](http://ohtustats2016.herokuapp.com/)

Omat palautukset palauttava sivu on __http://ohtustats2016.herokuapp.com/students/012345678/submissions__ (vaihda __012345678__ omaksi opiskelijanumeroksesi). Palvelin palauttaa tietosi [json-muodossa](http://en.wikipedia.org/wiki/JSON)

Tavoitteena on tehdä ohjelma, joka ottaa komentoriviparametrina opiskelijanumeron ja tulostaa palautettujen tehtävien statistiikan ihmisystävällisessä muodossa.

Ohjelmassa tarvitaan muutamaa kirjastoa:

* HTTP-pyynnön tekemiseen <http://hc.apache.org/httpcomponents-client-4.4.x/>
* InputStreamin merkkijonoksi muuttamiseen [http://commons.apache.org/io/](http://commons.apache.org/io/)
* json-muotoisen merkkijonon muuttaminen olioksi [http://code.google.com/p/google-gson/](http://code.google.com/p/google-gson/)

Liitä projektisi pom.xml:n seuraavat riippuvuudet

* commons-httpclient, Commons IO, gson
* löydät riippuvuuksien tiedot osoitteesta [http://mvnrepository.com/](http://mvnrepository.com/)
* Ainakin seuraavat versiot on todettu yhteensopiviksi ja toimivaksi projektin koodin kanssa: commons-httpclient 3.1, Commons IO 2.0, gson 2.1

Ota mallia edellisen tehtävän projektista ja määrittele maven-compiler-plugin käyttämään javan versiota 1.6

Voit ottaa projektisi pohjaksi seuraavan tiedoston:

```java

import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;

public class Main {

    public static void main(String[] args) throws IOException {
        String studentNr = "012345678";
        if ( args.length>0) {
            studentNr = args[0];
        }

        String url = "http://ohtustats2016.herokuapp.com/students/"+studentNr+"/submissions";

        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);
        client.executeMethod(method);

        InputStream stream =  method.getResponseBodyAsStream();

        String bodyText = IOUtils.toString(stream);

        System.out.println("json-muotoinen data:");
        System.out.println( bodyText );

        Gson mapper = new Gson();
        Submission[] subs = mapper.fromJson(bodyText, Submission[].class);
        
        System.out.println("Oliot:");
        for (Submission submission : subs) {
            System.out.println(submission);
        }

    }
}
```

*HUOM:* jos teet koodia NetBeansilla, kirjastoja ei ehkä tunnisteta ennenkuin teet clean and buildin ja NB lataa ne mavenin repositoriosta koneellesi.

Tehtäväpohjassa on valmiina luokan <code>Submission</code> koodin runko. Gson-kirjaston avulla json-muotoisesta datasta saadaan taulukollinen <code>Submission</code>-olioita, joissa jokainen olio vastaa yhden viikon palautusta. Tee luokkaan oliomuuttuja (sekä tarvittaessa getteri ja setteri) jokaiselle json-datassa olevalle kentälle, jota ohjelmasi tarvitsee. Kentät _a1_, _a2_ jne vastaavat viikolla tehtyjä yksittäisiä tehtäviä.

Tee kuitenkin ohjelmastasi tulostusasultaan miellyttävämpi, esim. seuraavaan tyyliin:

<pre>
opiskelijanumero 012345678

 viikko 1: tehtyjä tehtäviä yhteensä: 9, aikaa kului 3 tuntia, tehdyt tehtävät: 1 2 3 4 5 6 7 9 11 
 viikko 2: tehtyjä tehtäviä yhteensä: 6, aikaa kului 4 tuntia, tehdyt tehtävät: 1 2 3 6 7 8  

yhteensä: 15 tehtävää 7 tuntia
</pre>


## 3. lisää mavenia: jar joka sisältää kaikki riippuvuudet

* tehdään äskeisen tehtävän projektista jar-tiedosto komennolla <code>mvn install</code>
* suoritetaan ohjelma komennolla <code>java -cp tiedostonNimi.jar ohtu.Main</code>
* mutta ohjelma ei toimikaan, tulostuu:

``` java
Exception in thread "main" java.lang.NoClassDefFoundError: org/apache/commons/httpclient/HttpMethod
Caused by: java.lang.ClassNotFoundException: org.apache.commons.httpclient.HttpMethod
at java.net.URLClassLoader$1.run(URLClassLoader.java:202)
at java.security.AccessController.doPrivileged(Native Method)
at java.net.URLClassLoader.findClass(URLClassLoader.java:190)
at java.lang.ClassLoader.loadClass(ClassLoader.java:306)
at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:301)
at java.lang.ClassLoader.loadClass(ClassLoader.java:247)
Could not find the main class: ohtu.Main.  Program will exit.
```


Mistä on kyse?
* ohjelman riippuvuuksia eli projekteja commons-httpclient, Commons IO ja gson vastaavat jar-tiedostot eivät ole käytettävissä, joten ohjelma ei toimi
* saamme generoitua ohjelmasta jar-tiedoston joka sisältää myös riippuvuudet mavenin assembly-pluginin avulla
* lisää pom.xml:n plugineihin seuraava:


```xml
<build>
        <plugins>
 
            <!-- ... -->
 
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.2.1</version>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>
 
        </plugins>
 
    </build>
```

komennolla <code>mvn assembly:assembly</code> syntyy koko ohjelman sisältävä "standalone"-jar-tiedosto:

<pre>
$ java -cp TyhjaProjekti2-1.0-jar-with-dependencies.jar ohtu.Main 012345678
 
opiskelijanumero 012345678

 viikko 1: tehtyjä tehtäviä yhteensä: 9, aikaa kului 3 tuntia, tehdyt tehtävät: 1 2 3 4 5 6 7 9 11 
 viikko 2: tehtyjä tehtäviä yhteensä: 6, aikaa kului 4 tuntia, tehdyt tehtävät: 1 2 3 6 7 8  

yhteensä: 15 tehtävää 7 tuntia
</pre>


Riippuvuudet sisältävä jar-voidaan myös tehdä käyttämällä mavenin [shade-pluginia](http://maven.apache.org/plugins/maven-shade-plugin/) Shade-pluginin avulla saadaan itseasiassa aikaan "helppokäyttöisempi" jar, joka voidaan käynnistää määrittelemättä main-metodin sisältävää luokkaa.

Määrittele shade-pluginille mainClassin sijainti lisäämällä pom.xml:ääsi seuraava:

```xml
<build>
        <plugins>
 
            <!-- ... -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>ohtu.Main</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
 
    </build>
```

Saat luotua jar:in komennolla <code>mvn package</code>, ja ohjelman suoritus tapahtuu komennolla <code>java -jar tiedostonnimi.jar</code>


## 4. git: monta etärepositorioa

Tehtävässä oletetaan, että sinulla on 2 repositoria GitHub:issa. Käytetään niistä nimiä A ja B

* kloonaa A koneellesi
* liitä B paikalliselle koneelle kloonaamaasi repositorioon etärepositorioksi
  * katso ohje [http://git-scm.com/book/en/Git-Basics-Working-with-Remotes](http://git-scm.com/book/en/Git-Basics-Working-with-Remotes)
* nyt paikallisella repositoriollasi on kaksi remotea A (nimellä origin) ja B (määrittelemälläsi nimellä)
  * tarkista komennolla git remote -v että näin todellakin on
* pullaa B:n master-haaran sisältö paikalliseen repositorioon (komennolla <code>git pull beelleantamasinimi master</code>)
  * pullaus siis aiheuttaa sen, että etärepositorin master-haara mergetään lokaalin repositorion masteriin, tämä voi aiheuttaa konfliktin, jos, niin ratkaise konflikti!
* pushaa paikallisen repositorion sisältö A:han eli originiin

## 5. git: monta etärepositorioa jatkuu

jatketaan edellistä

* liitä paikalliseen repositorioosi (edellisen tehtävän A) remoteksi repositorio `git://github.com/mluukkai/ohtu2016.git` esim. nimellä ohtu
* ei pullata ohtu-repossa olevaa tavaraa lokaaliin, vaan tehdään sille oma träkkäävä branchi:
  * anna komennot <code>git fetch ohtu</code> ja <code>git checkout -b ohtu-lokaali ohtu/master</code>
  * varmista komennolla <code>git branch</code> että branchi (nimeltä ohtu-lokaali) syntyi ja että olet branchissa
  * tee komento <code>ls</code> niin näet, että olet todellakin ohtu-repon lokaalissa kopiossa
* siirretään (tai otetaan mukaan, alkuperäinen ei häviä) ohtu-lokaali:n hakemisto viikko2 paikallisen repon masteriin:
  * palaa master-branchiin komennolla <code>git checkout master</code>
  * liitä branchin ohtu-lokaali hakemisto viikko2 masteriin komennolla <code>git checkout ohtu-lokaali viikko2</code>      
  * varmista että hakemisto on nyt staging-alueella komennolla <code>git status</code>
  * committaa
  * nyt sait siirrettyä sopivan osan toisen etärepositorion tavarasta lokaaliin repositorioon!
* pushaa lokaalin repositorion sisältö sekä originiin että B:hen


## 6. git: tägit

Tee tämä tehtävä repositorioon, jonka palautat

* Lue [http://git-scm.com/book/en/Git-Basics-Tagging](http://git-scm.com/book/en/Git-Basics-Tagging) (kohdat signed tags ja verifying tags voit skipata)
* tee tägi nimellä tagi1 (lightweight tag riittää)
* tee kolme committia (eli 3 kertaa muutos+add+commit )
* tee tägi nimellä tagi2
* katso <code>gitk</code>-komennolla miltä historiasi näyttää
* palaa tagi1:n aikaan, eli anna komento <code>git checkout tagi1</code>
  * varmista, että tagin jälkeisiä muutoksia ei näy
* palaa nykyaikaan
  * tämä onnistuu komennolla <code>git checkout master</code>
* lisää tägi edelliseen committiin
  * onnistuu komennolla <code>git tag tagi1b HEAD^</code> , eli HEAD^ viittaa nykyistä "headia" eli olinpaikkaa historiassa edelliseen committiin
  * joissain windowseissa muoto <code>HEAD^</code> ei toimi, sen sijasta voit käyttää muotoa <code>HEAD~</code>
  * tai katsomalla commitin tunniste (pitkä numerosarja) joko komennolla <code>git log</code> tai gitk:lla
* kokeile molempia tapoja, tee niiden avulla kahteen edelliseen committiin tagit (tagi1a ja tagi1b)
* katso komennolla <code>gitk</code> miltä historia näyttää

Tagit eivät mene automaattisesti etärepositorioihin. Pushaa koodisi githubiin siten, että myös tagit siirtyvät mukana. Katso ohje [täältä](http://git-scm.com/book/en/Git-Basics-Tagging#Sharing-Tags)

Varmista, etä tagit siirtyvät Githubiin:

![kuva](https://github.com/mluukkai/ohtu2015/raw/master/images/viikko4-1.png)

## 7. Spring WebMVC

Tarkastellaan edelliseltä viikolta tutun toiminnallisuuden tarjoamaa esimerkkiprojektia joka löytyy repositorion <https://github.com/mluukkai/ohtu2016> hakemistossa __viikko4/LoginWeb2__

**Hae projekti ja käynnistä se komennolla**

```sh
mvn jetty:run
```

Jetty on keyvt HTTP-palvelin ja Servlettien ajoympäristö. Projektiin on konfiguroitu Jetty Maven-pluginiksi. Jos kaikki menee hyvin, on sovellus nyt käynnissä ja voit käyttää sitä web-selaimella osoitteesta [http://localhost:8090](http://localhost:8090) eli paikalliselta koneeltasi portista 8090.

Jos koneellasi on jo jotain muuta portissa 8090, voit konfiguroida sovelluksen käynnistymään johonkin muuhun porttiin esim. 9999:n seuraavasti:

```sh
mvn -D jetty.port=9999 jetty:run
```

SpringWebMVC:stä tällä kurssilla ei tarvitse ymmärtää. Kannattaa kuitenkin vilkaista tiedostoa __ohtu.OhtuController.java__, joka sisältää sovelluksen eri osoitteisiin tulevista kutsuista huolehtivan koodin. Kontrolleri käyttää __AuthenticationService__-luokkaa toteuttamaan kirjautumisen tarkastuksen ja uusien käyttäjien luomisen. Kontrolleri delegoi www-sivujen renderöinnin hakemiston __WebPages/WEB-INF-views__ alla oleville jsp-tiedostoille.

Eli tutustu nyt sovelluksen rakenteeseen ja toiminnallisuuteen. Saat sammutettua sovelluksen painamalla konsolissa ctrl+c tai ctrl+d.

## 8. Selenium, eli web-selaimen simulointi ohjelmakoodista

Web-selaimen simulointi onnistuu mukavasti [Selenium WebDriver](http://docs.seleniumhq.org/projects/webdriver/) -kirjaston avulla. Edellisessä tehtävässä olevassa projektissa on luokassa __ohtu.Tester.java__ pääohjelma, jonka koodi on seuraava:

```java
public static void main(String[] args) {
    WebDriver driver = new HtmlUnitDriver();

    driver.get("http://localhost:8090");
    System.out.println( driver.getPageSource() );
    WebElement element = driver.findElement(By.linkText("login"));
    element.click();

    System.out.println("==");

    System.out.println( driver.getPageSource() );
    element = driver.findElement(By.name("username"));
    element.sendKeys("pekka");
    element = driver.findElement(By.name("password"));
    element.sendKeys("akkep");
    element = driver.findElement(By.name("login"));
    element.submit();

    System.out.println("==");
    System.out.println( driver.getPageSource() );
}
```

**Käynnistä sovellus edellisen tehtävän tapaan komentoriviltä.** Varmista selaimella että sovellus on päällä.

Aja Tester.java:ssa oleva ohjelma. Esim. NetBeansilla tämä onnistuu valitsemalla tiedoston nimen kohdalta oikealla hiiren napilla "Run file".

Katso mitä ohjelma tulostaa.

Tester-ohjelmassa luodaan alussa selainta simuloiva olio __WebDriver driver__. Tämän jälkeen "mennään" selaimella osoitteeseen __localhost:8090__ ja tulostetaan sivun lähdekoodi. Tämän jälkeen haetaan sivulta elementti, jossa on linkkiteksti __login__ eli

```java
WebElement element = driver.findElement(By.linkText("login"));
```

Linkkielementtiä klikataan ja jälleen tulostetaan sivun lähdekoodi. Seuraavaksi etsitään sivulta elementti, jonka nimi on __username__, kyseessä on lomakkeen input-kenttä, ja ohjelma "kirjoittaa" kenttään komennolla sendKeys() nimen "pekka".

Tämän jälkeen täytetään vielä salasanakenttä ja painetaan lomakkeessa olevaa nappia. Lopuksi tulostetaan vielä sivun lähdekoodi.

Ohjelma siis simuloi selaimen käyttöskenaarion, jossa kirjaudutaan sovellukseen.

Jos koneessasi on Firefox, **muuta Testerin pääohjelman rivi 11** muotoon:

```java
WebDriver driver = new FirefoxDriver();
```

Suorita tester uudelleen. Jos koneesi Firefox-versio on yhteensopiva käytössä olevan selenium-version kanssa, sinun pitäisi nähdä Firefoxin käynnistyvän ja seleniumin "suorittavan" koodin skenaario Firefoxilla. 

**Muuta nyt koodia siten, että läpikäyt seuraavat skenaariot** (jos FirefoxFriver ei toimi koneellasi, käytä alunperin koodissa käytettyä <code>HtmlUnitDriver</code>:ia):

* epäonnistunut kirjautuminen: oikea käyttäjätunnus, väärä salasana
* epäonnistunut kirjautuminen: ei-olemassaoleva käyttäjätunnus
* uuden käyttäjätunnuksen luominen
* uuden käyttäjätunnuksen luomisen jälkeen tapahtuva ulkoskirjautuminen sovelluksesta

*HUOM:* salasanan varmistuskentän (confirm password) nimi on __passwordConfirmation__

## 9. Web-sovelluksen testaaminen: easyB+Selenium

Pääsemme jälleen käyttämään viime viikolta tuttua [easyB:tä](https://github.com/mluukkai/ohtu2016/blob/master/web/easyb.md). Hakemistosta Other Test Sources/easyb löytyy valmiina User storyn *User can log in with valid username/password-combination* määrittelevä story. Yksi skenaarioista on valmiiksi mäpätty koodiin. Täydennä kaksi muuta skenaariota.

Testit on konfiguroitu suoritettavaksi samalla tavalla kuin viime viikon easyB-tehtävässä. Huomaa, että voit testeissäkin halutessasi käyttää <code>FirefoxDriver</code>:ia. 

**Huom:** Firefox ei jostain syystä toimi easyB:n kanssa jos testejä suoritetaan Travisissa. Jos joudut esim. miniprojektissa käyttämään FirefoxDriveria, joudut Travisin takia toteuttamaan testit jUnitilla.

## 10. Web-sovelluksen testaaminen osa 2

Kuten viime viikolta muistamme, toinen järjestelmän toimintaa määrittelevä User story on *A new user account can be created if a proper unused username and a proper password are given*

Löydät tämän Storyn easyB-pohjan viime viikon tehtävistä. Kopioi story projektiisi ja tee skenaarioista suoritettavia kirjoittamalla niihin Seleniumin avulla (edellisen tehtävän tyyliin) sovellusta testaavaa koodia. Muista lisätä story-tiedostoon Seleniumin vaatimat importit!

**Huomioita**
* voit tehdä __Tester.java__:n tapaisen pääohjelman sisältävän luokan jos haluat/joudut debuggaamaan testiä. Toinen hyvä debuggaustapa on FirefoxDriverin käyttö
* Uuden käyttäjän luomisen pohjalla käytettävään luokkaan __UserData__ on määritelty validoinnit käyttäjätunnuksen muodon ja salasanan oikeellisuuden tarkastamiseksi. Eli toisin kuin viime viikolla, ei AuthenticationServicen tarvitse suorittaa validointeja.
* Skenaarion "can login with succesfully generated account" mäppäävän koodin kirjoittaminen ei ole täysin suoraviivaista. Koska luotu käyttäjä kirjautuu automaattisesti järjestelmään, joudut kirjaamaan käyttäjän ensin ulos ja kokeilemaan tämän jälkeen että luotu käyttäjä pystyy kirjautumaan sivulle uudelleen.
* Huomaa, että jos luot käyttäjän yhdessä testissä, et voi luoda toisessa testissä samannimistä käyttäjää uudelleen!

## tehtävien kirjaaminen palautetuksi

tehtävien kirjaus:

* Kirjaa tekemäsi tehtävät [tänne](http://ohtustats2016.herokuapp.com) 
  * huom: tehtävien palautuksen deadline on su 17.4. klo 23.59

palaute tehtävistä:

* Lisää viikon 1 tehtävässä 11 forkaamasi repositorion omalla nimelläsi olevaan hakemistoon tiedosto nimeltä viikko4
* tee viime viikon tehtävän tapaan pull-request
  * anna tehtävistä palautetta avautuvaan lomakkeeseen
  * huom: jos teet tehtävät alkuviikosta, voi olla, että edellistä pull-requestiasi ei ole vielä ehditty hyväksyä ja et pääse vielä tekemään uutta requestia