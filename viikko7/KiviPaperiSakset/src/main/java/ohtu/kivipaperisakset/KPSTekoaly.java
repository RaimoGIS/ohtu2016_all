package ohtu.kivipaperisakset;

import java.util.Scanner;

public class KPSTekoaly extends KPS {
    
    Tekoaly tekoaly = new Tekoaly();
    
    public void pelaa() {
        
        ekanSiirto = ensimmaisenSiirto();
        tokanSiirto = tekoaly.annaSiirto();
        System.out.println("Tietokone valitsi: " + tokanSiirto);
        
        while (onkoOkSiirto(ekanSiirto) && onkoOkSiirto(tokanSiirto)) {
            kirjaaSiirto(ekanSiirto, tokanSiirto);

            ekanSiirto = ensimmaisenSiirto();
            tokanSiirto = tekoaly.annaSiirto();
            
            System.out.println("Tietokone valitsi: " + tokanSiirto);
            tekoaly.asetaSiirto(ekanSiirto);
        }     
        tuomarointi();
    }
}