/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ohtu.kivipaperisakset;

import java.util.Scanner;

/**
 *
 * @author rpiiroin
 */
public abstract class KPS {
    
    public static final Scanner scanner = new Scanner(System.in);
    
    Tuomari tuomari = new Tuomari();
    String ekanSiirto;
    String tokanSiirto;
  
    public void kirjaaSiirto (String ekanSiirto, String tokanSiirto) {
        tuomari.kirjaaSiirto(ekanSiirto, tokanSiirto);
        System.out.println(tuomari);
        System.out.println();
    }

    public String ensimmaisenSiirto () {
        System.out.print("Ensimmäisen pelaajan siirto: ");
        ekanSiirto = scanner.nextLine();
        return ekanSiirto;
    }
    
    public void tuomarointi () {
        System.out.println("\nKiitos!");
        System.out.println(tuomari);
    }
    
    public static boolean onkoOkSiirto(String siirto) {
        return "k".equals(siirto) || "p".equals(siirto) || "s".equals(siirto);
    }
}
