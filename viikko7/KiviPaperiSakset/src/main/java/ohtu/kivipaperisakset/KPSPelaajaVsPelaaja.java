package ohtu.kivipaperisakset;

import java.util.Scanner;

// Kun extendataan KPS niin peritään kaikki sen metodit
// Siirrä PelaajaVSPelaaja kohtaiset toiminnallisuudet siis tänne
// Tämä on template metodi. Eli KPS on template jota laajennetaan yksittäistapauksiksi
public class KPSPelaajaVsPelaaja extends KPS {

    public void pelaa() {

        ekanSiirto = ensimmaisenSiirto();

        System.out.print("Toisen pelaajan siirto: ");
        tokanSiirto = scanner.nextLine();
        
        while (onkoOkSiirto(ekanSiirto) && onkoOkSiirto(tokanSiirto)) {
            
            kirjaaSiirto(ekanSiirto, tokanSiirto);
            ekanSiirto = ensimmaisenSiirto();
            
            System.out.print("Toisen pelaajan siirto: ");
            tokanSiirto = scanner.nextLine();
        }
        tuomarointi();
    }
    

}