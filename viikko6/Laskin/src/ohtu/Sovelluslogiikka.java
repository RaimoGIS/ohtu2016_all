package ohtu;

public class Sovelluslogiikka {
 
    private int tulos;
 
    public void plus(int luku) {
        System.out.println("ajettu plussa" + luku);
        tulos += luku;
    }
     
    public void miinus(int luku) {
        tulos -= luku;
    }
    
    public void nollaa() {
        
        tulos = 0;
    }
 
    public int tulos() {
        System.out.println("tama on tulos" + tulos);
        return tulos;
    }
    
    public void palautaedellinen(int edellinen) {
        tulos = edellinen;
    }

    
}