/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ohtu;

import javax.swing.JTextField;

/**
 *
 * @author rpiiroin
 */
class Erotus implements Komento {
    private final Sovelluslogiikka sovellus;
    private final JTextField tuloskentta;
    private final JTextField syotekentta;
    int arvo = 0;
    int laskunTulos = 0;
    int edellinenTulos = 0;
    
    public Erotus(Sovelluslogiikka sovellus, JTextField tuloskentta, JTextField syotekentta) {
        this.sovellus = sovellus;
        this.tuloskentta = tuloskentta;
        this.syotekentta = syotekentta;    
    }

    @Override
    public void suorita() {
        
        try {
            this.arvo = Integer.parseInt(syotekentta.getText());
        } catch (Exception e) {
        }
        
        
        edellinenTulos = sovellus.tulos();
        
        sovellus.miinus(arvo);
        
       
        laskunTulos = sovellus.tulos();
        
        syotekentta.setText("");
        tuloskentta.setText("" + laskunTulos);
        
        
        
    }

    @Override
    public void peru() {
        
        tuloskentta.setText("" + edellinenTulos);
        sovellus.palautaedellinen(edellinenTulos);
        //System.out.println("edellinen tilanne oli" + edellinenTulos);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
