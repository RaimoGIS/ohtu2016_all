/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics.matcher;

import java.lang.reflect.Method;
import statistics.Player;

/**
 *
 * @author rpiiroin
 */
public class HasFewerThan implements Matcher {

    private int value; // raja-arvo
    private String fieldName; // kentta josta haetaan?
    
    // Konstruktorissa annetaan raja-arvo ja kategoria, joka vastaa fieldNamea
    public HasFewerThan(int value, String category) {
        this.value = value;
        fieldName = "get"+Character.toUpperCase(category.charAt(0))+category.substring(1, category.length());
    }  
    
    @Override
    public boolean matches(Player p) {
        try {                                    
            Method method = p.getClass().getMethod(fieldName); // Hyödynnetään reflect metodia
            int playersValue = (Integer)method.invoke(p); // playersValue int on jotain metodi luokan tuotoksesta
            return playersValue<value; // koska haluamme fewer than tuloksen kerromme että palautetaan vain raja-arvon alittavat pelaajat
            
        } catch (Exception ex) {
            System.out.println(ex);
            throw new IllegalStateException("Player does not have field "+fieldName.substring(3, fieldName.length()).toLowerCase());
        }   
    }
    
}
