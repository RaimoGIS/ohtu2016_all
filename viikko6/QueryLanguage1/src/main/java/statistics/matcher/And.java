package statistics.matcher;

import statistics.Player;

public class And implements Matcher {

    private Matcher[] matchers;

    public And(Matcher... matchers) {
        this.matchers = matchers;
    }
    
    
    @Override
    // Seuraavassa otetaan yksi pelaaja kerrallaan sisään
    // ja loopataan kaikki matcherit läpi
    // Palautetaan false
    public boolean matches(Player p) {
        //int x = 0;
        for (Matcher matcher : matchers) {
            //x+=1;
            //System.out.println("montako kertaa " +x); // eli loopataan aina annettu määrä matchereita läpi
            if (!matcher.matches(p)) {
                return false;
            }
        }

        return true;
    }
}
