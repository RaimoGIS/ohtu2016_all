/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package statistics.matcher;

import statistics.matcher.PlaysIn;

/**
 *
 * @author rpiiroin
 */
public class QueryBuilder {
    Matcher matcher;
    
    public QueryBuilder() {
        matcher = new alwaysTrue();
    }
    
    
    public Matcher matcher(){
        return matcher;
    }

    /*
    public build() {
        
    }*/

    
    public QueryBuilder playsIn(String team) {
        this.matcher = new And(new PlaysIn(team), matcher);
        return this;
    }
    
    public QueryBuilder hasAtLeast(int value, String category) {
        this.matcher = new And(new HasAtLeast(value, category), matcher);
        return this;
    }    

    public QueryBuilder hasFewerThan(int value, String category) {
        this.matcher = new And(new HasFewerThan(value, category),matcher);
        return this;
    }  

    public Object or(Matcher m1, Matcher m2) {
        this.matcher = new Or(m1,m2);
        return this;
    }
}
