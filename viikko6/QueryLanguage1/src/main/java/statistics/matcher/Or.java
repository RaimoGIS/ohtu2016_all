package statistics.matcher;

import statistics.Player;

public class Or implements Matcher {

    private Matcher[] matchers;

    public Or(Matcher... matchers) {
        this.matchers = matchers;
    }

    
    // Eli OR:ssa riittää että tulee yksikin matchi niin silloin kelpaa
    @Override
    public boolean matches(Player p) {
        for (Matcher matcher : matchers) {
            if (matcher.matches(p)) {
                //System.out.println("FALSE:" + matcher);
                return true;
            }
        }
        //System.out.println("Nama lisataan listaan");
        return false;
    }
}
