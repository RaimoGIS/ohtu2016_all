package statistics;

import statistics.matcher.QueryBuilder;
import statistics.matcher.*;

public class Main {
    public static void main(String[] args) {
        Statistics stats = new Statistics(new PlayerReaderImpl("http://nhlstats-2013-14.herokuapp.com/players.txt"));
        
        
        QueryBuilder query = new QueryBuilder();
        
        
        System.out.println("Andeilla");
        
        Matcher m = query.hasAtLeast(30, "goals")
                         .hasAtLeast(30, "assists").playsIn("NYR").matcher();
        
        for (Player player : stats.matches(m)) {
            System.out.println( player );
        }


		System.out.println("Only in master");
        System.out.println("Only in experiment branch");
        

        /*
        System.out.println("Orina");
        
        Matcher m1 = query.playsIn("PHI")
                          .hasAtLeast(10, "goals")
                          .hasFewerThan(20, "assists").matcher();

        Matcher m2 = query.playsIn("EDM")
                          .hasAtLeast(50, "points").matcher();

        Matcher m = query.or(m1, m2).matcher();
        
        for (Player player : stats.matches(m)) {
            System.out.println( player );
        }*/
        
        /*
        
        Matcher m = new And( new HasAtLeast(10, "goals"),
                             new HasAtLeast(10, "assists"),
                             new PlaysIn("PHI")
        );
        
        for (Player player : stats.matches(m)) {
            System.out.println( player );
        }
        
        
        System.out.println("new query with AND and hasFewerThan");
        
        m = new And( new HasFewerThan(10, "goals"),
                             new HasFewerThan(10, "assists"),
                             new PlaysIn("PHI")
        );
        
        for (Player player : stats.matches(m)) {
            System.out.println( player );
        }
        
        
        System.out.println("test only one switch");
        
        m = new HasFewerThan(2,"goals");
        for (Player player : stats.matches(m)) {
            System.out.println( player );
        }
        
        
        System.out.println("new query OR and HasAtLeast");
        
        m = new Or( new HasAtLeast(50, "goals"),
                             new HasAtLeast(50, "assists")
        );
        
        for (Player player : stats.matches(m)) { // tämä lista sisältää vain ne pelaajat jotka täyttivät ehdot
            System.out.println( player );
        }
        
        // Testataan vielä NOT operaattori
        
        System.out.println("new query with NOT and HasAtLeast");
        
        m = new Not( new HasFewerThan(50, "goals")
        );
        
        for (Player player : stats.matches(m)) { // tämä lista sisältää vain ne pelaajat jotka täyttivät ehdot
            System.out.println( player );
        }*/
        
    }
}
