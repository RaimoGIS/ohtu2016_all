/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ohtu.data_access;

import static java.awt.SystemColor.text;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import ohtu.domain.User;

/**
 *
 * @author ramip_000
 */
public class FileUserDAO implements UserDao {
    
    Scanner lukija;
    private List<User> users;
    private String kayttaja;
    private String salasana;
    
    public FileUserDAO(String tiedosto) throws FileNotFoundException {
        
        this.lukija = new Scanner(new File(tiedosto));
        String rivi = lukija.nextLine();
        lukija.close();
        String[] jaettu = rivi.split(",");
        
        this.kayttaja = jaettu[0];
        //System.out.println("kayttaja " + kayttaja);
        this.salasana = jaettu[1];
        //System.out.println("salasana " + salasana);
        users = new ArrayList<User>();
        users.add(new User(kayttaja, salasana));
    }      
    
    @Override
    public List<User> listAll() {
        return users;
    }

    @Override
    public User findByName(String name) {
        for (User user : users) {
            if (user.getUsername().equals(name)) {
                return user;
            }
        }

        return null;
    }

    @Override
    public void add(User user) {
        users.add(user);
    }
    
    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }
}
