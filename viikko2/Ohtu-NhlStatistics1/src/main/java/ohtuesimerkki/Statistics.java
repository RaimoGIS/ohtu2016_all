package ohtuesimerkki;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Statistics {
    
    private Reader reader; // Määrittele tässä uusi Reader reader. Tämä on mahdollista koska reader on interface!?
    private List<Player> players;

    public Statistics(Reader reader) {

        this.reader = reader; // Tässä tehdään readerista uusi instanssi?
        //PlayerReader reader = new PlayerReader("http://nhlstatistics.herokuapp.com/players.txt");
        
        players = reader.getPlayers();       // Tämä hakee playerit PlayReader luokan kautta. 
                                             // Eli ei tarvitse enää viitata PlayerReader luokkaan suoraan!
    }

    public Player search(String name) {
        for (Player player : players) {
            if (player.getName().contains(name)) {
                return player;
            }
        }

        return null;
    }

    public List<Player> team(String teamName) {
        ArrayList<Player> playersOfTeam = new ArrayList<Player>();
        
        for (Player player : players) {
            if ( player.getTeam().equals(teamName)) {
                playersOfTeam.add(player);
            }
        }
        
        return playersOfTeam;
    }

    public List<Player> topScorers(int howMany) {
        Collections.sort(players);
        ArrayList<Player> topScorers = new ArrayList<Player>();
        Iterator<Player> playerIterator = players.iterator();
        
        while (howMany>=0) {
            topScorers.add( playerIterator.next() );            
            howMany--;
        }
        
        return topScorers;
    }

}
