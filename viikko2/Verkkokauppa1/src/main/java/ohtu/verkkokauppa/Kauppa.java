package ohtu.verkkokauppa;

public class Kauppa {

    //private Varasto varasto;
    private VarastoIo varasto;
    //private Pankki pankki;
    private PankkiIo pankki;
    private Ostoskori ostoskori;
    //private Viitegeneraattori viitegeneraattori;
    private ViitegeneraattoriIo viitegeneraattori;
    private String kaupanTili;

    /*public Kauppa() {
        varasto = Varasto.getInstance();
        pankki = Pankki.getInstance();
        viitegeneraattori = Viitegeneraattori.getInstance();
        kaupanTili = "33333-44455";
    }*/
    public Kauppa(VarastoIo varasto, PankkiIo pankki, ViitegeneraattoriIo viitegeneraattori) {
        //this.varasto = Varasto.getInstance();
        //this.pankki = Pankki.getInstance();
        //this.viitegeneraattori = Viitegeneraattori.getInstance();
        this.varasto = varasto;
        this.pankki = pankki;
        this.viitegeneraattori = viitegeneraattori;
        
    }

    public void aloitaAsiointi() {
        ostoskori = new Ostoskori();
    }

    public void poistaKorista(int id) {
        Tuote t = varasto.haeTuote(id); 
        varasto.palautaVarastoon(t);
    }

    public void lisaaKoriin(int id) {
        if (varasto.saldo(id)>0) {
            Tuote t = varasto.haeTuote(id);             
            ostoskori.lisaa(t);
            varasto.otaVarastosta(t);
        }
    }

    public boolean tilimaksu(String nimi, String tiliNumero) {
        int viite = viitegeneraattori.uusi();
        int summa = ostoskori.hinta();
        
        return pankki.tilisiirto(nimi, viite, tiliNumero, kaupanTili, summa);
    }

}
